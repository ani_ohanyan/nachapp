<?php

use yii\db\Migration;

/**
 * Handles the creation for table `goals`.
 */
class m160520_150521_create_goals extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('goals', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('goals');
    }
}
