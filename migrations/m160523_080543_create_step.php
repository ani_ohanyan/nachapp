<?php

use yii\db\Migration;

/**
 * Handles the creation for table `step`.
 */
class m160523_080543_create_step extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('step', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11)->notNull(),
            'name' => $this->string()->notNull(),
            'parent_id' => $this->integer(11),
            'category_id' => $this->integer(11),
            'type' => $this->string(),
            'created_at' => $this->timestamp(),
        ]);

        // creates index for column `author_id`
        $this->createIndex(
            'idx-step-category_id',
            'step',
            'category_id'
        );

        // add foreign key for table `notes`
        $this->addForeignKey(
            'fk-step-category_id',
            'step',
            'category_id',
            'categories',
            'id',
            'CASCADE'
        );

        // creates index for column `user_id`
        $this->createIndex(
            'idx-step-user_id',
            'step',
            'user_id'
        );

        // add foreign key for table `notes`
        $this->addForeignKey(
            'fk-step-user_id',
            'step',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        // drops foreign key for table `notes`
        $this->dropForeignKey(
            'fk-step-category_id',
            'step'
        );

        // drops index for column `note_id`
        $this->dropIndex(
            'idx-step-category_id',
            'step'
        );

        // drops foreign key for table `notes`
        $this->dropForeignKey(
            'fk-step-user_id',
            'step'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-step-user_id',
            'step'
        );

        $this->dropTable('step');
    }

}
