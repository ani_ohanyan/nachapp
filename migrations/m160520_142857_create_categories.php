<?php

use yii\db\Migration;

/**
 * Handles the creation for table `categories`.
 */
class m160520_142857_create_categories extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('categories', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11)->notNull(),
            'name' => $this->string()->notNull(),
            'description' => $this->string(),
            'archive ' => $this->smallInteger(1)->defaultExpression(0)
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-category-user_id',
            'categories',
            'user_id'
        );

        // add foreign key for table `categories`
        $this->addForeignKey(
            'fk-category-user_id',
            'categories',
            'category_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `categories`
        $this->dropForeignKey(
            'fk-category-user_id',
            'categories'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-category-user_id',
            'categories'
        );

        $this->dropTable('categories');
    }
}
