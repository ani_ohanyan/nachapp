<?php

use yii\db\Migration;

/**
 * Handles the creation for table `files`.
 */
class m160523_070846_create_files extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('files', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'path' => $this->string(),
            'size' => $this->integer(),
            'note_id' => $this->integer(11),
            'created_at' => $this->timestamp(),
        ]);

        // creates index for column `author_id`
        $this->createIndex(
            'idx-files-note_id',
            'files',
            'note_id'
        );

        // add foreign key for table `notes`
        $this->addForeignKey(
            'fk-files-note_id',
            'files',
            'note_id',
            'notes',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `notes`
        $this->dropForeignKey(
            'fk-files-note_id',
            'files'
        );

        // drops index for column `note_id`
        $this->dropIndex(
            'idx-files-note_id',
            'files'
        );

        $this->dropTable('files');
    }
}