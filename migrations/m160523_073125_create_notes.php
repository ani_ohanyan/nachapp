<?php

use yii\db\Migration;

/**
 * Handles the creation for table `notes`.
 */
class m160523_073125_create_notes extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('notes', [
            'id' => $this->primaryKey(),
            'content' => $this->string(),
            'archived' => $this->smallInteger(1)->defaultValue(0),
            'pin_top' => $this->smallInteger(1)->defaultValue(0),
            'attachment_id' => $this->integer(11),
            'type' => $this->text(),
//            'type' => $this->enum(['folder', 'step']),
            'created_at' => $this->timestamp(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('notes');
    }
}
