<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        "css/nachapp/main1c4d.css?v148",
        "css/style.css",
        "css/main.css",
        "css/nachapp/jquery-ui1c4d.css?v148",
//    <link rel="shortcut icon" href="../images.nachapp.com/favicon1c4d.png?v148"/>
//    <link rel="icon" sizes="196x196" href="../images.nachapp.com/icon-1961c4d.png?v148">
//    <link rel="apple-touch-icon" sizes="76x76" href="../images.nachapp.com/apple-touch-icon-ipad1c4d.png?v148">
//    <link rel="apple-touch-icon" sizes="120x120" href="../images.nachapp.com/apple-touch-icon-iphone-retina1c4d.png?v148">
//    <link rel="apple-touch-icon" sizes="152x152" href="../images.nachapp.com/apple-touch-icon-ipad-retina1c4d.png?v148">
//    <link rel="apple-touch-icon" href="../images.nachapp.com/apple-touch-icon-iphone1c4d.png?v148">
    "css/pickadate/default.date.css",
    "css/pickadate/default.time.css"
    ];
    public $js = [
        "js/nachapp/pog3zpm.js",
        "assets/fe4b1b7a/jquery.js",
        "js/dragzone/dropzone.js",
        "js/script.js",
        "js/pickadate/picker.js",
        "js/pickadate/picker.date.js",
        "js/pickadate/picker.time.js",
        "js/pickadate/legacy.js",
        "js/moment.min.js",
        "assets/9a8be6d4/yii.js",
        "assets/9a8be6d4/yii.activeForm.js",
    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
