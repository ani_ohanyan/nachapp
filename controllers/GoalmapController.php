<?php

namespace app\controllers;

use app\models\Step;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Categories;
use app\models\User;
use app\models\Goals;

class GoalmapController extends Controller {

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actionIndex() {
        $data['category'] = Categories::findAll(['user_id'=>Yii::$app->user->getId()]);
        $steps = Step::findAll(['user_id'=>Yii::$app->user->getId()]);
		
        foreach($steps as $key =>$step) {
            if ($step['parent_id'] == 0 && $step['type'] == 'step') {
                $data['inbox'][] = $step;
            } else {
                $goals[] =  $step;
            }
        }
		
        foreach($goals as $key => $array){
            foreach($goals as $goal) {
                if($goal['parent_id'] == $array['id']){
                    $goals['folders'] = $step['parent_id'];
                }
            }
        }
		
        return $this->render('goalmap', $data);
    }

    public function actionAdd_step() {
        if(Yii::$app->request->post()){
            $insert = new Step();
            $insert['parent_id'] = Yii::$app->request->post('parent_id');
            $insert['name'] = Yii::$app->request->post('name');
            $insert['type'] = Yii::$app->request->post('type');
            $user = User::findIdentity(Yii::$app->user->getId());
            $insert['user_id'] = $user['id'];
            $insert->save();
            if($insert){
                return $insert->id;
            }
        }
        return false;
    }
}