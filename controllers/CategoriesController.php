<?php

namespace app\controllers;

use app\models\Categories;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;

class CategoriesController extends Controller {

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    

    public function actionIndex() {
        $data['category'] = Categories::findAll(['user_id'=>Yii::$app->user->getId()]);
        return $this->render('categories', $data);
    }

    public function actionAdd_category(){
        if(Yii::$app->request->post()){
            $name = Yii::$app->request->post('new_text');
            $user = User::findIdentity(Yii::$app->user->getId());
            $insert = new Categories();
            $insert->user_id = $user['id'];
            $insert->name = $name;
            
            if($insert->save()){
                return $insert->id;
            }
        }
        return false;
    }

    public function actionEdit_category(){
        if(Yii::$app->request->post()){
            $data_id = Yii::$app->request->post('data_id');
            $data['name'] = Yii::$app->request->post('edit_name');
            $data['description'] = Yii::$app->request->post('edit_desc');
            $checked = Yii::$app->request->post('checked');
            if($checked == 'checked'){
                $data['archive'] = 1;
            } else {
                $data['archive'] = 0;
            }
			
            $res = Categories::updateAll($data,['id'=>$data_id]);
            if($res){
                return true;
            }
        }
        return false;
    }

    public function actionDelete_category(){
        if(Yii::$app->request->post()){
            $data_id = Yii::$app->request->post('data_id');
			
            if(Categories::deleteAll(['id'=>$data_id])){
                return true;
            }
        }
        return false;
    }
}