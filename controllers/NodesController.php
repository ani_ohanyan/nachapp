<?php

namespace app\controllers;

use app\models\Step;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
class NodesController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex($id=false)
    {
        $data['data'] = Step::findOne(['id'=>$id]);
        $data['parent'] = Step::findOne(['id'=>$data['data']['parent_id']]);
        return $this->render('index', $data);

    }

}
