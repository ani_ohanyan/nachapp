<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\User;
use app\models\Profile;

class ProfileController extends Controller {

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $data['profile'] = Profile::findOne(['user_id'=>Yii::$app->user->getId()]);
        return $this->render('profile', $data);
    }

    public function actionClose(){
        return $this->render('close');
    }

    public function actionSave_mobile(){
        $p_number = Yii::$app->request->post('p_number');
        $profile = Profile::findOne(['user_id'=>Yii::$app->user->getId()]);
        if($profile){
            if(Profile::updateAll(['phone_number'=> $p_number],['user_id'=>$profile['user_id']])){
                return true;
            }
        } else {
            $user = User::findIdentity(Yii::$app->user->getId());
            $insert = new Profile();
            $insert->user_id = $user['id'];
            $insert->phone_number = $p_number;
            
            if($insert->save()){
                return true;
            }
        }
        return false;
    }
}