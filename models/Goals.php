<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "goals".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property integer $note_id
 * @property integer $image_id
 * @property integer $complate
 * @property integer $category_id
 * @property string $due_date
 *
 * @property Categories $category
 * @property User $user
 */
class Goals extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goals';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'name', 'note_id', 'image_id', 'category_id'], 'required'],
            [['user_id', 'note_id', 'image_id', 'complate', 'category_id'], 'integer'],
            [['due_date'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'note_id' => 'Note ID',
            'image_id' => 'Image ID',
            'complate' => 'Complate',
            'category_id' => 'Category ID',
            'due_date' => 'Due Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }
}
