<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notes".
 *
 * @property integer $id
 * @property string $content
 * @property integer $archived
 * @property integer $pin_top
 * @property integer $attachment_id
 * @property string $type
 * @property string $created_at
 *
 * @property Files[] $files
 */
class Notes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['archived', 'pin_top', 'attachment_id'], 'integer'],
            [['type'], 'string'],
            [['created_at'], 'safe'],
            [['content'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Content',
            'archived' => 'Archived',
            'pin_top' => 'Pin Top',
            'attachment_id' => 'Attachment ID',
            'type' => 'Type',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(Files::className(), ['note_id' => 'id']);
    }
}
