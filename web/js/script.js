$(document).ready(function(){
    window.formated_date = '';
    Dropzone.autoDiscover = false;
    /*  hide shown elements  */
    $(document).on('click', function(event){
        $('.dropdown--shortcuts').css('display','none');
        $('.js-show-parts').hide();
        $('.js-area-input').hide();
        $('.js-help-popout-next').hide();
        $(this).show();
        $('.js-parts').hide();
        $('.node-add__input').val('');
        $('.js-part').removeClass('node-add__part--active');
        $('.js-expand-due').removeClass('js-expand-due-click');
        $('.js-button-custom-date').show();
        $('.js-date-time-picker').parent().hide();
        $('.js-help-popout-mobile').hide(); //on profile page
        $('.js-help-popout-categories').hide();
        $('.dropdown--help-snip').hide(); //notes view page
    });
/*
 *
 * to_do page start
 * */

    $(document).on('click','.measure-row__input-rec-date', function(event){
        var _this = $(this);
        $(this).pickadate({
            format: 'mm/dd/yyyy',
            formatSubmit: 'mm/dd/yyyy',
            hiddenName: true,
            onSet: function(context) {
                if(context.select){
                        var date = new Date(context.select);
                    formated_date = moment(date).format('ddd, D MMMM');
                    _this.parents('.js-expand-due').find('.dropdown__date-preview').addClass('js-preview dropdown__date-preview--good')
                    _this.parents('.js-expand-due').find('.dropdown__date-preview').find('span').text(formated_date);
                    _this.parents('.js-expand-due').find('.dropdown__date-preview').find('.js-custom-save ').removeClass('link-disabled');
                    $('.js-input-time').attr('disabled', false);
                } else {
                    $('.js-input-time').attr('disabled', true);
                    _this.parents('.js-expand-due').find('.dropdown__date-preview').removeClass('js-preview dropdown__date-preview--good')
                    _this.parents('.js-expand-due').find('.dropdown__date-preview').find('span').text('');
                    _this.parents('.js-expand-due').find('.dropdown__date-preview').find('.js-custom-save ').addClass('link-disabled');
                }
            }
        });

    });

    /* time */
    $(document).on('keyup', '.js-input-time', function(){
        var match  = formatTime($(this).val());
        $(this).parents('.js-expand-due').find('.dropdown__date-preview').find('span').text('');
        if(match){
            console.log(match);
            $(this).parents('.js-expand-due').find('.dropdown__date-preview').find('span').append(formated_date + ' @ '+ match);
        } else {
            $(this).parents('.js-expand-due').find('.dropdown__date-preview').find('span').append(formated_date);
        }
    });

    /*to do page print */
    $('.js-print').on('click', function(){
        print();
    });


    $(document).on('click', '.js-area-input',function(event){
        event.preventDefault();
        event.stopPropagation();
    });

    /* Save new to_do */
    $('.todos-quick-add__slot').find('.js-save-button').on('click', function(){
        var new_to_do = $(this).parents('.node-add').find('.node-add__input').val();
        if($.trim(new_to_do) != ''){
            $(this).parents('.node-add').find('.js-show-parts').hide();
            $(this).parents('.node-add').find('.js-area-input').hide();
            var repeate = '';
            $('.todos-quick-add__slot').find('.node-add__input').val('');
            if ( new_to_do.indexOf('*') > -1 ){
                repeate = '<span class="hint--left" data-hint="Repeat: sporadic"> <i class="icon-cw"></i> </span>';
            }
            $('.js-list-next').find('.js-todo-children').append('<li class="todo" style="display: list-item;">' +
                '<a class="todo__box todo__box--check "> ' +
                '<i class="js-status-icon"></i></a>  ' +
                '<div class="todo__date">'+repeate+'' +
                '   <a class="todo__options"> <i class="icon-down-dir"></i> </a>' +
                '</div>  ' +
                '        <div class="todo__desc">  ' +
                '            <a href="#" data-hint="Goal: Inbox" class="todo__name hint--right">'+ new_to_do +'\</a>  ' +
                '        <div class="js-streak streak" data-heat="0" style="display: none"><i class="icon-cw">  ' +
                '            </i><span></span></div>  ' +
                '        <span class="todo__icons">  ' +
                '</span>  ' +
                '</div>  ' +
                '            <div class="dropdown dropdown--shortcuts" style="top: 165px; right: 10px; display: none;"> ' +
                '            <div class="dropdown__item dropdown__item--heading">Shortcuts</div> ' +
                '            <a class="dropdown__button js-button-todo-toggle" data-status="completed" data-value="true"> ' +
                '            <i class="dropdown__icon icon-check"></i> Complete to-do  </a> ' +
                '        <a class="dropdown__button js-button-expand" data-expand="due"> ' +
                '            <i class="dropdown__icon"></i> Set next due... <i class="dropdown__nest-indicator icon-down-dir"></i>  ' +
                '</a> ' +
                '            <div class="js-expand-due"> ' +
                '            <a class="dropdown__button dropdown__button--nested js-button-due" data-target="today"> ' +
                '            <i class="dropdown__icon"></i> Today ' +
                '            </a>  ' +
                '            <a class="dropdown__button dropdown__button--nested js-button-due" data-target="tomorrow"> ' +
                '            <i class="dropdown__icon"></i> Tomorrow ' +
                '            </a> ' +
                '            <a class="dropdown__button dropdown__button--nested js-button-custom-date" data-postpone="no"> ' +
                '            <i class="dropdown__icon"></i> Custom...  </a> ' +
                '            </div> ' +
                '            <a class="dropdown__button js-button-delete"> ' +
                '            <i class="dropdown__icon"></i> ' +
                '            <span class="text--delete">Delete</span> ' +
                '            </a> ' +
                '            </div> ' +
                '            </li>');
        }
    })

    /* show reference in creating new to_do*/
    $('.js-show-parts').on('click', function(){
        $(this).hide();
        $('.js-parts').show();
    });

    /* clicking on shortcuts  in creating new to_do*/
    $('.js-part').on('click', function(){
        if($(this).hasClass('node-add__part--active')){
            return false;
        } else {
            $(this).addClass('node-add__part--active');
            var symbol = $(this).find('span').text();
            $('.node-add__input').val($('.node-add__input').val() + symbol);
        }
    });

    /* dropdown part show in to_do part*/
    $(document).on('click', '.todo__options', function(e){
        var top = $(this).offset().top + 35 - $('.app-action-bar').height();
        e.preventDefault();
        e.stopPropagation();
        if($(this).parents('.todo').find('.dropdown--shortcuts').css('display') == 'none'){
            $('.dropdown--shortcuts').css({'opacity':0, 'display':'none'});
            $(this).parents('.todo').find('.dropdown--shortcuts').css({'opacity':1, 'display':'block', 'top':top, 'right': '10px'});
        } else{
            $(this).parents('.todo').find('.dropdown--shortcuts').css({'opacity':0, 'display':'none'});
        }
        $('.js-expand-due').removeClass('js-expand-due-click');
        $('.js-expand-due').hide()
    });
    /* complete to_do */
    $(document).on('click', '.todo__box--check', function(){
        complete_to_do($(this));
    });
    $(document).on('click', '.todo .js-button-todo-toggle', function(){
        complete_to_do($(this));
    });


    /* 'set next due' dropdown toggle*/
    $(document).on('click','.js-button-expand', function(e){
        e.preventDefault();
        e.stopPropagation();
        $(this).parents('.dropdown--shortcuts').find('.js-expand-due').toggleClass('js-expand-due-click');
        $(this).parents('.dropdown--shortcuts').find('.js-expand-due').show();
    });

    /* ? icon */
    $('.help-prompt').on('click', function(e){
        e.stopPropagation();
        $('.js-help-popout-next').toggle();
        var top = $('.help-prompt').offset().top + 20;
        var left = $('.help-prompt').offset().left -50;
        $('.js-help-popout-next').offset({ top: top, left: left});
    });

    /* Set next custom due */
    $(document).on('click','.js-button-custom-date', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).hide();
        $(this).parents('.todo').find('.js-expand-due').append('<div>' +
            '<div class="js-date-time-picker" style="padding: 18px 5px 10px;">' +
            '<div class="date-time-picker"> ' +
            '<div class="hint--bottom hint--always hint--error" style="display: inline-block; vertical-align: top">' +
            '<input class="form__input measure-row__input-rec-date js-input-date" type="text" value="" placeholder="Date" style="width: 100px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-right-width: 0px; padding-left: 8px; padding-right: 8px;">' +
            '</div>' +
            '<a class="js-pickadate date-time-picker__cal-button picker__input" data-value="" id="P1421535362" type="text" aria-haspopup="true" aria-expanded="false" aria-readonly="false" aria-owns="P1421535362_root">' +
            '<i class="icon-calendar"></i>' +
            '</a>   ' +
            '<div style="display: inline-block;">' +
            '<input class="form__input js-input-time" type="text" placeholder="Time" value="" style="width: 50px; padding-left: 8px; padding-right: 8px;" disabled="disabled">  ' +
            '</div>  ' +
            '</div></div>' +
            '<div class="dropdown__date-preview js-preview">' +
            '<span class="">&nbsp;</span>' +
            '<a class="js-custom-save hint--bottom link-disabled" data-hint="Save">' +
            '<i class="icon-check"></i></a>' +
            '</div>' +
            '</div>');
    });

    $(document).on('click', '.dropdown', function(e){
        e.preventDefault();
        e.stopPropagation();
    });

    /* delete modal show*/
    $(document).on('click','.todo .js-button-delete', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).parents('.todo').append('<div class="modal modal--visible">' +
            '<div class="modal__backdrop js-cancel"></div>' +
            '<ul class="modal__container" style="position: absolute; top: 202px;">' +
            '<li class="modal__header">' +
            '<i class="icon-trash"></i> Confirm delete' +
            '</li>' +
            '<div class="js-content">' +
            '<p>Are you sure you want to permanently delete the step <strong></strong>?</p>' +
            '<p class="js-checking" style="display: none; font-size: 12px; color: #aaa; text-transform: uppercase">' +
            '<i class="icon-spin4 animate-spin loading-orb"></i> Checking contents' +
            '</p>' +
            '</div>' +
            '<div style="margin: 20px 0 10px">' +
            '<a href="javascript:;" class="button button--cta js-confirm">Delete</a>' +
            '<a href="javascript:;" class="button js-cancel">Cancel</a>' +
            '</div>' +
            '</ul>' +
            '</div> ')
    });
    /* cancel */
    $(document).on('click','.todo .js-cancel', function(){
        $('.modal').remove();
    });

    /* delete */
    $(document).on('click', '.js-confirm', function(){
        $(this).parents('.todo').remove();
        $('.modal').remove();
        $('.todos').each(function() {
            if($.trim($(this).find('.js-todo-children').html()) == ''){
                $(this).hide();
            }
        });
    });

    /* set due date */
    $(document).on('click', '.js-button-due', function(){
        if( $(this).attr('data-target') == 'today'){
            var date = 'Today ';
            var selector = $('.js-list-today');
        } else if( $(this).attr('data-target') == 'tomorrow'){
            var date = 'Tomorrow';
            var selector = $('.js-list-later');
        } else if($(this).attr('data-target') == 'none'){
            var date = '';
            var selector = $('.js-list-next');
        }
        $('.js-button-custom-date').show();
        $('.js-date-time-picker').parent().hide();
        due_date($(this), date, selector);
    });
    $(document).on('click', '.js-custom-save', function(){
        var selected_date = $('.measure-row__input-rec-date').val();
        var today = moment().format('MM/DD/Y');
        var last_week = moment().subtract(7, 'days').format('MM/DD/Y');
        var prev_week = moment().add(7, 'days').format('MM/DD/Y');
        if(selected_date > today && selected_date <= prev_week){
            var date = moment(selected_date).format('dddd');
            var selector = $('.js-list-later');
        } else if (selected_date < today && selected_date >= last_week){
            var selector = $('.js-list-overdue');
            var date = 'last '+ moment(selected_date).format('ddd');
        } else if(selected_date == today){
            var date = "Today";
            var selector = $('.js-list-today');
        } else {
            var date = "";
            var selector = '';
        }
        $('.js-button-custom-date').show();
        $('.js-date-time-picker').parent().hide();
        due_date($(this), date, selector);
    });

/*
 *
 * to_do page finish
 * */

/*
*
* goal page start
* */
    /* add folder */
    $('body').on('click','.node-add__area-type-prompts a:last-of-type', function(){
        event.stopPropagation();
        $('.js-area-input').hide();
        $(this).parents('.node-add').find('.js-area-input').find('.js-input-emblem').removeClass('icon-flag bg--step');
        $(this).parents('.node-add').find('.js-area-input').find('.js-input-emblem').addClass('bg--goal icon-globe');
        $(this).parents('.node-add').find('.js-area-input').css('opacity',1);
        $(this).parents('.node-add').find('.js-area-input').show();
    });
    $('body').on('click','.node-add__area-type-prompts a:first-of-type, .node-add__area-type-prompts i:first-of-type', function(){
        if($(this).parents('.js-area-type-prompts').find('a:first').text() == 'Add step'){
            event.stopPropagation();
            $('.js-area-input').hide();
            $(this).parents('.node-add').find('.js-area-input').find('.js-input-emblem').addClass('icon-flag bg--step');
            $(this).parents('.node-add').find('.js-area-input').find('.js-input-emblem').removeClass('bg--goal icon-globe');
            $(this).parents('.node-add').find('.js-area-input').css('opacity',1);
            $(this).parents('.node-add').find('.js-show-parts').show();
            $(this).parents('.node-add').find('.js-area-input').show();
        }
    });

    /* save sub_folder or sub_step*/
    $(document).on('click', '#js-page-goals .js-save-button', function(){
        var name = $(this).parents('.node-add').find('.js-area-input').find('.node-add__input').val();
        //var parent_id = $(this).parents('.node-summary').find('.js-self').attr('data_id');
        var parent_id = $(this).parents('.node-summary__add').siblings('.js-list').attr('data_id');
        if ($.trim(name) != ''){
            $(this).parents('.node-add').find('.js-area-input').hide();
            $(this).parents('.node-add').find('.js-area-input').find('.node-add__input').val('');
            if($(this).parents('.js-area-input').find('.js-input-emblem').hasClass('icon-globe')){
                var type = 'folder';
                console.log($(this).parents('.node-summary__add').siblings('.js-list').html());
                $(this).parents('.node-summary__add').siblings('.js-list').append('<li class="node-summary js-draggable-node js-node-id-69278 ui-draggable" style="display: list-item;"> ' +
                    '<div class="js-self node-summary__self ui-droppable"><a class="node-summary__toggle js-toggle icon-down-dir"></a> <i class="node-summary__emblem emblem bg--goal icon-globe js-sort-handle emblem--sort-handle js-emblem-menu"></i> ' +
                    '<div class="js-main node-summary__main"> ' +
                    '<div class="node-summary__editable-name js-editable-name">' +
                    '<a href="#nodes/69278" class="node-summary__name js-name-value editable-stat" data-name="asdasdas">'+name+'</a></div> ' +
                    '<span class="js-icons node-summary__icons">' +
                    '<span class="js-category node-summary__category" style="display: none;"><i class="icon-folder"></i><span></span></span> </span> ' +
                    '</div> </div>' +
                    '            <div class="node-summary__children js-children"> ' +
                    '<ul class="js-list-archived" style="display: none"></ul> ' +
                    '<ul class="js-list" ></ul> ' +
                    '<div class="node-summary__add js-add"> ' +
                    '<div class="node-add node-add--summary ui-droppable"><a class="node-summary__toggle icon-right-dir" style="visibility: hidden"></a> ' +
                    '<div class="js-area-type-prompts node-add__area-type-prompts"><i class="js-prompt-emblem emblem emblem--plus node-summary__emblem icon-plus node-add__prompt-emblem"></i> ' +
                    '<a href="javascript:;">Add step</a> <span style="opacity: 0.3; margin: 0 3px">|</span> <a href="javascript:;">Add sub-goal</a></div> ' +
                    '<div class="js-area-input node-add__area-input" style="display: none"> ' +
                    '<div style="display: table; border-collapse: collapse; width: 100%"> ' +
                    '<div style="display: table-row"> ' +
                    '<div style="display: table-cell; width: 32px"><i class="js-input-emblem emblem bg--goal icon-globe node-add__input-emblem" style="cursor: pointer"></i></div> ' +
                    '<div style="display: table-cell"><input class="node-add__input js-input behat-node-add-input" value="" maxlength="255"></div> ' +
                    '<a style="display: table-cell; width: 32px; text-align: center;" data-hint="Save" class="js-save-button hint--bottom"><i class="icon-check"></i></a></div> ' +
                    '</div> ' +
                    '<a class="node-add__show-parts js-show-parts" style="display: none"> ' +
                    '<i class="icon-down-open"></i> Show shortcuts </a> ' +
                    '<div class="js-parts" style="display: none"> ' +
                    '<ul class="node-add__parts"> ' +
                    '<li class="js-part js-part-due node-add__part node-add__part--due hint--bottom" data-shortcut="^"><i class="icon-calendar"></i> ' +
                    '<span></span></li> ' +
                    '<li class="js-part js-part-repeat node-add__part node-add__part--repeat" data-shortcut="*"><i class="icon-cw"></i> <span></span> ' +
                    '</li> ' +
                    '<li class="js-part js-part-reminder node-add__part node-add__part--reminder hint--bottom" data-shortcut="!"><i class="icon-mail"></i> ' +
                    '<span></span></li> ' +
                    '</ul> ' +
                    '<a class="node-add__parts-help" href="/help/guides/add-shortcuts" target="_blank">Shortcut reference ?</a></div> ' +
                    '</div> ' +
                    '</div> </div> </div> </li>');
            } else {
                var type = 'step';
                $(this).parents('.node-summary__add').siblings('.js-list').append('<li class="node-summary js-draggable-node js-node-id-70072 ui-draggable" style="display: list-item;">' +
                    '            <div class="js-self node-summary__self ui-droppable">' +
                    '                <a class="node-summary__toggle js-toggle icon-right-dir" style="visibility: hidden"></a>' +
                    '                   <i class="node-summary__emblem emblem icon-flag bg--step js-sort-handle emblem--sort-handle js-emblem-menu"></i> ' +
                    '           <div class="js-main node-summary__main">' +
                    '            <div class="node-summary__editable-name js-editable-name">' +
                    '            <div class="node-summary__editable-name js-editable-name">' +
                    '            <div class="editable-stat editable-stat--node-summary">' +
                    '            <span class="js-area-read">' +
                    '            <a class="node-summary__name js-name-value" href="#" data-name="anasun\">'+name+'\</a>' +
                    '            <a class="editable-stat__button editable-stat__edit-prompt js-stat-edit hint--right" data-hint="Edit">' +
                    '            <i class="icon-pencil"></i>' +
                    '            </a>   </span>' +
                    '            <span class="js-area-edit" style="display: none">' +
                    '            <span class="js-error hint--right hint--always hint--error">' +
                    '            <input name="behat-editable-stat-input" type="text" class="editable-stat__input js-stat-input behat-editable-stat-input" value="anasun\" maxlength="255">' +
                    '        </span>' +
                    '        <a class="editable-stat__button js-stat-save hint--right" data-hint="Save">' +
                    '            <i class="icon-check"></i>' +
                    '            </a>' +
                    '            <a class="editable-stat__button js-stat-discard hint--right" data-hint="Cancel">' +
                    '            <i class="icon-cancel"></i>' +
                    '            </a> </span> </div> </div> </div>' +
                    '<span class="js-icons node-summary__icons">' +
                    '<span class="hint--right" data-hint="1 Note">' +
                    '<i class="icon-doc-alt"></i>' +
                    '            <span class="js-category node-summary__category" style="display: none;">' +
                    '<i class="icon-folder"></i>          ' +
                    '  <span></span>    ' +
                    '        </span> </span>            </div>' +
                    '            </div>' +
                    '            <div class="dropdown dropdown--shortcuts dropdown--visible">' +
                    '            <div class="dropdown__item dropdown__item--heading">Shortcuts</div>' +
                    '            <a class="dropdown__button js-button-rename">' +
                    '            <i class="dropdown__icon icon-pencil"></i> Rename' +
                    '            </a>' +
                    '            <a class="dropdown__button js-button-todo-toggle" data-status="completed" data-value="true">' +
                    '            <i class="dropdown__icon icon-check"></i> Complete to-do' +
                    '            </a>' +
                    '        <a class="dropdown__button js-button-expand" data-expand="due">' +
                    '            <i class="dropdown__icon"></i> Set next due...' +
                    '        <i class="dropdown__nest-indicator icon-down-dir"></i>' +
                    '            </a>' +
                    '            <div class="js-expand-due" style="display: none;">' +
                    '            <a class="dropdown__button dropdown__button--nested js-button-due" data-target="today">' +
                    '            <i class="dropdown__icon"></i> Today' +
                    '            </a>' +
                    '            <a class="dropdown__button dropdown__button--nested js-button-due" data-target="tomorrow">' +
                    '            <i class="dropdown__icon"></i> Tomorrow' +
                    '            </a>' +
                    '            <a class="dropdown__button dropdown__button--nested js-button-custom-date" data-postpone="no">' +
                    '            <i class="dropdown__icon"></i> Custom...' +
                    '            </a>' +
                    '            </div>' +
                    '            <a class="dropdown__button js-button-delete">' +
                    '            <i class="dropdown__icon"></i>' +
                    '            <span class="text--delete">Delete</span>' +
                    '            </a>' +
                    '            </div>' +
                    '            </li>');
                }

            $.ajax({
                type     :'POST',
                cache    : false,
                url  : 'goalmap/add_step',
                data: {name:name, parent_id:parent_id, type:type}
            });
        }
    });


    /* tabs */
    $(document).on('click','.js-tab', function(){
        $('.js-tab').removeClass('tab-strip__tab--selected');
        $(this).addClass('tab-strip__tab--selected');
    })

    /* toggle folders */
    $(document).on('click','#js-page-goals .node-summary__toggle', function(){
        $(this).parents('.node-summary__self').siblings('.node-summary__children').toggle();
        $(this).toggleClass('icon-down-dir');
        $(this).toggleClass('icon-right-dir');
    })

    /* icon flag goals page */

    $(document).on('click','.icon-flag, .icon-globe', function(e){
        //console.log(e.which);
        e.preventDefault();
        e.stopPropagation();
        var _this = $(this);
        if($(this).parent().siblings('.dropdown--shortcuts').css('display') == 'none'){
            $('.dropdown--shortcuts').css({'opacity':0, 'display':'none'});
            var top = _this.offset().top + 30;
            var left = _this.offset().left - 80;
            $(this).parent().siblings('.dropdown--shortcuts').css({'opacity':1, 'display':'block', 'top': top, 'left':left});
        }
        else{
            $(this).parent().siblings('.dropdown--shortcuts').css({'opacity':0, 'display':'none'});
        }
        $('.js-expand-due').removeClass('js-expand-due-click');
    });

    $('.icon-flag .icon-globe').on('mousedown', function(e){
        e.preventDefault();
        if(e.which == 3){
            $(this).trigger( "click" );
        };
    })
    /* inbox hide/show */

    $('.icon-toggle').on('click', function(){
        $(this).toggleClass('icon-down-dir');
        $(this).toggleClass('icon-right-dir');
        if($(this).hasClass('icon-down-dir')){
            $('.js-children').css('display', 'block');
        }
        else if($(this).hasClass('icon-right-dir')){
            $('.js-children').css('display', 'none');
        }
    });

    /*rename*/
    $('.js-filters-exterior').find('.js-button-rename').on('click', function(){
        $(this).parents('.dropdown--shortcuts').siblings('.node-summary__self').find('.js-area-read').hide();
        $(this).parents('.dropdown--shortcuts').siblings('.node-summary__self').find('.js-area-edit').css('display', 'inline');
        $(this).parent('.dropdown').hide()
    });

    /* save rename */
    $('.js-filters-exterior').find('.js-stat-save').on('click', function(){
        var selector = $(this).siblings('span').find('input').val();
        $('.js-area-read').css('display', 'inline');
        $('.js-area-edit').css('display', 'none');
        $(this).parents('.js-area-edit').siblings('.js-area-read').find('a').html(selector);
    });

    /* cancel */
    $('.js-filters-exterior').find('.js-stat-discard').on('click', function(){
        $(this).parents('.js-draggable-node').find('.js-area-read').show();
        $(this).parents('.js-draggable-node').find('.js-area-edit').hide();
        $(this).parent('.dropdown').hide();
    });

    /* complate to-do */
    $(document).on('click', '.js-draggable-node .js-button-todo-toggle', function(e){
        if( $(this).attr('data-value') == 'false'){
            $(this).attr('data-value', 'true');
            $(this).siblings('.js-button-expand').show();
            $(this).html('<i class="dropdown__icon icon-check"></i> Complete to-do');
            $(this).parents('.js-list').find('.node-summary__emblem').removeClass('bg--completed-step');
            $(this).parents('.js-list').find('.node-summary__emblem').addClass('bg--step');
            $(this).parents('.js-list').find('.node-summary__emblem').removeClass('icon-check');
            $(this).parents('.js-list').find('.node-summary__emblem').addClass('icon-flag');
        } else {
            $(this).attr('data-value', 'false');
            $(this).siblings('.js-button-expand').hide();
            $(this).html('<i class="dropdown__icon"></i> Uncomplete to-do');
            $(this).parents('.js-list').find('.node-summary__emblem').addClass('bg--completed-step');
            $(this).parents('.js-list').find('.node-summary__emblem').removeClass('bg--step');
            $(this).parents('.js-list').find('.node-summary__emblem').addClass('icon-check');
            $(this).parents('.js-list').find('.node-summary__emblem').removeClass('icon-flag');
        }
        $(this).parents('.dropdown--shortcuts').hide();
    });




/*
 *
 * goal page finish
 * */
/*
 *
 * profile page start
 * */

    /* edit mobile number*/
    $(document).on('click','.js-stat-mobile  .editable-stat__edit-prompt', function(){
        $('.js-area-read').hide();
        $('.js-area-edit').show();
    });
    $('.js-stat-mobile').find('.js-stat-discard').on('click', function(){
        $('.js-area-read').show();
        $('.js-area-edit').hide();
        $('.js-area-edit').find('.behat-editable-stat-input').val('');
    });
    $('.js-stat-mobile').find('.behat-editable-stat-input').on('keyup', function(){
        if(validatePhone($(this).val()) || $(this).val() == ''){
            $(this).removeClass('editable-stat__input--invalid');
            $('.js-stat-save').removeClass('link-disabled');
            $(this).removeClass('editable-stat__input--invalid');
            $(this).addClass('editable-stat__input--valid');
            $(this).parent().removeAttr('data-hint');
        } else {
            $(this).addClass('editable-stat__input--invalid');
            $('.js-stat-save').addClass('link-disabled')
            $(this).addClass('editable-stat__input--invalid');
            $(this).removeClass('editable-stat__input--valid');
            $(this).parent().attr('data-hint', 'Must be valid mobile number including country code');
        }
    });

    /* save phone number */
    $('.js-stat-mobile').find('.js-stat-save').on('click', function(){
        if($(this).hasClass('link-disabled')){
        } else {
            var p_number = $(this).parents('.js-area-edit').find('.behat-editable-stat-input').val();
            $('.js-area-read').show();
            $('.js-area-edit').hide();
            $('.js-area-edit').find('.behat-editable-stat-input').val('');
            $(this).parents('.js-stat-mobile').find('.js-area-read').html(p_number+'<a class="editable-stat__button editable-stat__edit-prompt js-stat-edit hint--right" data-hint="Edit"> <i class="icon-pencil"></i></a>');
            $.ajax({
                type     :'POST',
                cache    : false,
                url  : 'profile/save_mobile',
                data: {p_number:p_number}
            })
        }
    });

    /* change avatar */
    $('.js-avatar-change').on('click', function(){
        var fileName = '';
       $(this).siblings('#avatar_input').trigger('click');
        $("input:file").change(function (){

        });
    });

    /*  help icon click */
    $('.js-help-prompt-mobile').on('click', function(){
        event.preventDefault();
        event.stopPropagation();
        $(this).siblings('.js-help-popout-mobile').toggle();
    });

    /* api key show */
    $('.js-show-api-key').on('click', function(){
        $('.js-api-key').show();
        $(this).hide();
    });

    /* show more */
    $('.lab .js-lab-more').on('click', function(){
        $(this).siblings('p').show();
        $(this).hide();
    });

/*
* search part start
*
* */
    /* ? icon */
    $('.icon-help-circled').on('click', function(e){
        e.stopPropagation();
        $('.js-help-popout-search').toggle();
        var left = $(this).offset().left +25;
        $('.js-help-popout-search').offset({ top:0, left: left});
    });

/*
*   categories part
*
* */

    /* ? click */
    $('.heading--divider .icon-help-circled').on('click', function(){
        var left = $(this).offset().left - $('.js-live-bg-exclusion').offset().left +25;
        var top = $(this).offset().top - 50;
        $(this).parents('.heading--divider').siblings('.js-help-popout-categories').css({ top: top, left: left});
        $(this).parents('.heading--divider').siblings('.js-help-popout-categories').toggle();
    });

    /* add new category */
    $('.padded-content .js-save-new').on('click', function(){
        var new_text = $(this).siblings('.js-new').val();
        var _this = $(this);
        $.ajax({
            type     :'POST',
            cache    : false,
            url  : 'categories/add_category',
            data: {new_text:new_text},
            success: function(res){
                if(res){
                    _this.siblings('.js-new').val('');
                    _this.siblings('.js-list').append('<li class="categories__item">' +
                        '<div class="js-cont-read">' +
                        '   <strong style="font-weight: 600">'+new_text+'</strong>' +
                        '   <a class="js-edit"> <i class="icon-pencil"></i> </a>' +
                        '</div>' +
                        '   <ul class="js-cont-edit form form--boxed" style="display: none">' +
                        '       <li class="form__row">' +
                        '           <label class="form__label">Category name</label> ' +
                        '           <input class="form__input js-edit-name" maxlength="40" value="'+new_text+'">' +
                        '       </li>' +
                        '       <li class="form__row">' +
                        '            <label class="form__label">Description</label>' +
                        '            <textarea class="form__input js-edit-desc" maxlength="255" style="height: 80px"></textarea>' +
                        '       </li>' +
                        '       <li class="form__row">' +
                        '            <input class="js-edit-archived" type="checkbox"> Archive this category' +
                        '        </li>' +
                        '        <li class="form__row" data_id = "'+res+'">' +
                        '            <a class="js-save button button--cta">Save</a>' +
                        '            <a class="js-discard button">Cancel</a>' +
                        '            <a class="js-delete button button--delete button--icon-left">' +
                        '                <i class="button__icon icon-trash"></i> Delete category' +
                        '            </a>' +
                        '       </li>' +
                        '   </ul>' +
                        '</li>');
                }
            }
        })
    });

    /* edit category */
    $(document).on('click', '.js-edit', function(){
        $(this).parents('.categories__item').find('.js-cont-edit').show();
        $(this).parents('.js-cont-read').hide();
    });

    /* cancel editing */
    $(document).on('click', '.js-discard', function(){
        $(this).parents('.js-cont-edit').hide();
        $(this).parents('.categories__item').find('.js-cont-read').show();
    });

    /* delete category */
    $(document).on('click', '.categories__item .js-delete', function(){
        var data_id = $(this).parents('.form__row').attr('data_id');
        if (confirm("Are you sure you want to delete this category? This won't affect any steps or goals under this category.") == true) {
            $(this).parents('.categories__item').remove();
        }

        $.ajax({
            type     :'POST',
            cache    : false,
            url  : 'categories/delete_category',
            data: { data_id:data_id}
        })

    });

    /* save editing category */
    $(document).on('click', '.js-cont-edit .js-save', function(){
        var edit_name = $(this).parents('.categories__item').find('.js-edit-name').val();
        var edit_desc = $(this).parents('.categories__item').find('.js-edit-desc').val();
        var data_id = $(this).parents('.form__row').attr('data_id');
        if($('.js-edit-archived').prop('checked')){
            var style = 'font-weight: normal; font-style: italic;'
            var checked = 'checked';
        } else {
            var style = 'font-weight: 600;';
            var checked = '';
        }

        $(this).parents('.categories__item').find('.js-cont-edit').hide();
        $(this).parents('.categories__item').html('<div class="js-cont-read">' +
            '   <strong style="'+style+'">'+edit_name+'</strong>' +
            '   <a class="js-edit"><i class="icon-pencil"></i></a> <br>'+edit_desc+' </div>' +
            '   <ul class="js-cont-edit form form--boxed" style="display: none">' +
            '     <li class="form__row"><label class="form__label">Category name</label>' +
            '        <input class="form__input js-edit-name" maxlength="40" value="'+edit_name+'"></li>' +
            '            <li class="form__row"><label class="form__label">Description</label>' +
            '            <textarea class="form__input js-edit-desc" maxlength="255" style="height: 80px">'+edit_desc+'</textarea>' +
            '            </li>' +
            '            <li class="form__row">' +
            '            <input class="js-edit-archived" type="checkbox" '+checked+'> Archive this category' +
            '        </li>' +
            '        <li class="form__row" data_id = "'+data_id+'" >' +
            '            <a class="js-save button button--cta">Save</a>' +
            '            <a class="js-discard button">Cancel</a>' +
            '            <a class="js-delete button button--delete button--icon-left">' +
            '               <i class="button__icon icon-trash"></i> Delete category' +
            '            </a>' +
            '       </li>' +
            '</ul>');


        $.ajax({
            type     :'POST',
            cache    : false,
            url  : 'categories/edit_category',
            data: {edit_name:edit_name,
                    edit_desc:edit_desc,
                    data_id:data_id,
                    checked:checked}
        })
    });


    /*
     *
     * History part start
     * */

    $('.journal-compose .js-journal-input').on('click', function(){
        $('.js-journal-buttons').show();
    });

    $('.journal-compose .js-journal-discard').on('click', function(){
        $('.js-journal-buttons').hide();
        $('.js-journal-input').val('')
    });

    /* edit note */
    $(document).on('click', '.history-bullets__list .js-edit-note', function(){
        $(this).parents('.history-bullets__bullet').find('.js-area-edit').show();
        $(this).parents('.js-area-content').hide();
    });

    /* cancel edit*/
    $('.history-bullets__list .js-cancel').on('click', function(){
        $(this).parents('.js-area-edit').hide();
        $(this).parents('.history-bullets__bullet').find('.js-area-content').show();
    });

    /* delete note */
    $(document).on('click', '.history-note__buttons .js-remove', function(){
        if (confirm("Are you sure you want to permanently delete this note?") == true) {
            $(this).parents('.history-bullets__bullet').remove();
        }
    });

    /* Show negative actions */
    $('.js-show-neg').on('click', function(){
        $('.js-date').not('.js-date-'+ moment().format("YYYY-MM-DD")).toggle();
        $('.js-steps-expand').on('click', function(){
            $(this).parents('.history-bullets__list').find('.history-bullets__bullet').not(':first').toggle();
        });
    });

    /* save new note */
    $('.journal-compose .js-journal-save').on('click', function(){
        var name = $('.js-journal-input').val();
        if($.trim(name) != '') {
            $('.js-journal-buttons').hide();
            $('.js-journal-input').val('');
            $('.history-bullets__list').append('<li class="history-bullets__bullet"><i class="emblem emblem--small bg--tracker icon-comment"></i>' +
                '            <undefined class="history-note js-note-id-10377">' +
                '            <div class="js-area-content">' +
                '            <div class="note-content js-note-content">' + name + '<a class="js-edit-note hint--bottom" data-hint="Edit note"><i class="icon-pencil text--link"></i></a></div>' +
                '           <div class="js-see-more-cont" style="display: none">' +
                '               <a class="js-see-more">Show more <span></span></a>' +
                '               <a class="js-edit-note hint--bottom" data-hint="Edit note"><i class="icon-pencil text--link"></i></a>' +
                '           </div>' +
                '        </div>' +
                '        <div class="js-area-edit" style="display: none; background: #fafafa; padding: 8px">' +
                '            <div style="margin: 6px 0 10px">' +
                '            <span class="hidden-mobile" style="margin-right: 5px;">Created</span>' +
                '            <div class="js-date-picker" style="display: inline-block;">' +
                '            <div class="date-time-picker">' +
                '            <div class="hint--bottom hint--always hint--error" style="display: inline-block; vertical-align: top">' +
                '               <input class="form__input measure-row__input-rec-date js-input-date" type="text" value="16 May 2016" placeholder="Date" style="width: 100px; border-top-right-radius: 0; border-bottom-right-radius: 0; border-right-width: 0;">' +
                '            </div>' +
                '            <a class="js-pickadate date-time-picker__cal-button picker__input" data-value="16 May 2016" id="P654122203" type="text" aria-haspopup="true" aria-expanded="false" aria-readonly="false" aria-owns="P654122203_root"><i class="icon-calendar"></i></a>' +
                '        </div>' +
                '        </div>' +
                '        </div>' +
                '        <textarea class="js-input form__input history-note__textarea" style="margin: 6px 0 2px;">' + name + '</textarea>' +
                '        <div class="history-note__buttons" style="margin-bottom: 3px;">' +
                '        <a class="button button--cta js-save">Save</a>' +
                '        <a class="button js-cancel">Cancel</a>' +
                '        <a class="button button--delete button--icon-solo js-remove">' +
                '           <i class="button__icon button__icon--solo icon-trash"></i> Delete note' +
                '        </a>' +
                '    </div>' +
                '   </div>' +
                '  </undefined>' +
                '</li>')
        }
    });

    /*
    *
    * nodes part
    * */

    /* dropdown menu toggle */
    $('.button--icon-solo').on('click', function(e){
        e.preventDefault();
        e.stopPropagation();
        $(this).parents('.node-detail-buttons').find('.dropdown--shortcuts').toggle();
    });

    /* archive */
    $('.js-button-archived-toggle').on('click', function(e){
        e.preventDefault();
        e.stopPropagation();
        $(this).parents('.node-detail-buttons').find('.dropdown--shortcuts').hide();
        if($(this).attr('data-action') == 'archive'){
            $(this).siblings('.js-button-expand').hide();
            $(this).attr('data-action','unarchive');
            $(this).html('<i class="dropdown__icon"></i> Unarchive')
            $('.js-completed-view').html('<div class="js-completed-wrapper completed "> ' +
                '<div class="completed__tick"> <i class="icon-check-alt"></i> </div> ' +
                '<div class="completed__rest"> <span class="completed__heading"> Step Completed </span> ' +
                '<div class="js-completed-area-view completed__area-view">   <span> Yesterday </span>   </div> ' +
                '<div class="js-completed-area-edit completed__area-edit" style="display: none"> ' +
                '<div class="js-completed-date-picker completed__date-picker"></div> ' +
                '<a class="js-completed-save completed__button hint--right"> <i class="icon-check"></i> Save </a> ' +
                '<a class="js-completed-cancel completed__button hint--right">' +
                ' <i class="icon-cancel"></i> Cancel </a> ' +
                '</div> ' +
                '</div> ' +
                '</div>')
        } else {
            $(this).siblings('.js-button-expand').show();
            $(this).attr('data-action','archive');
            $(this).html('<i class="dropdown__icon icon-archive"></i> Archive');
            $('.js-completed-view').html('');
        }
    });

    /* delete node */
    $('.node-detail__main .js-button-delete').on('click', function(e){
        e.preventDefault();
        e.stopPropagation();
        $('.node-detail__main .js-shortcut-menu').append('<div class="modal modal--visible" style="">' +
                        '        <div class="modal__backdrop js-cancel"></div>' +
                        '        <ul class="modal__container" style="position: absolute; top: 80px;">' +
                        '        <li class="modal__header"><i class="icon-trash"></i> Confirm delete</li>' +
                        '    <div class="js-content">' +
                        '        <p>Are you sure you want to permanently delete the step' +
                        '    <strong>ddd ^</strong>? <span class="js-also">You will also be deleting:</span>' +
                        '    </p>' +
                        '    <p class="js-checking" style="display: none; font-size: 12px; color: #aaa; text-transform: uppercase">' +
                        '        <i class="icon-spin4 animate-spin loading-orb"></i> Checking contents</p>' +
                        '    <ul class="bullet-list">' +
                        '        <li>4 notes</li>' +
                        '    <li>3 actions</li>' +
                        '    </ul>' +
                        '    </div>' +
                        '    <div style="margin: 20px 0 10px"><a href="javascript:;" class="button button--cta js-confirm">Delete</a>' +
                        '        <a href="javascript:;" class="button js-cancel">Cancel</a>' +
                        '    </div>' +
                        '  </ul>' +
                        '</div>')
    });

    /* confirm deleting */
    $(document).on('click', '.node-detail__main .js-confirm', function(){
        // ajax TODO
    });

    /* edit node */
    $(document).on('click','.timeline-note .timeline-note__button', function(){
        $(this).parents('.timeline-note').find('.timeline-note__edit').show();
        $(this).parents('.timeline-note').find('.js-area-content').hide();
    });

    /* cancel editing*/
    $(document).on('click','.timeline-note .js-cancel', function(){
        $(this).parents('.timeline-note').find('.timeline-note__edit').hide();
        $(this).parents('.timeline-note').find('.js-area-content').show();
    });

    /* delete editing*/
    $(document).on('click','.timeline-note .button--delete', function(){
        if (confirm("Are you sure you want to permanently delete this note?") == true) {
            $(this).parents('.js-notes').find('.js-add-buttons').show();
            $(this).parents('.timeline-row').remove();
        }
    });

    /* delete attachment */
    $(document).on('click','.timeline-note .js-delete', function () {
        if (confirm("Are you sure you want to permanently delete this attachment?") == true) {
            $(this).parents('.timeline-attachment--image').remove();
        }
        //if($(this).parents('.timeline-note__attachments').children().length == 0){
        //    $(this).parents('.timeline-row').hide();
        //} else {
        //    alert(8);
        //}
        //console.log($(this).parents('.timeline-note__attachments').children().length);
    });

    /* add a note */
    $('.js-notes .js-compose-prompt[data-type="text"]').on('click', function(){
        $(this).parents('.js-add-buttons').hide();
        $(this).parents('.timeline-container__inner').children('ul').prepend('<li class="timeline-row">' +
            ' <div class="timeline-row__mark"> ' +
            '<div class="timeline-row__bubble"> <i class="icon-plus"></i> </div> ' +
            '<div class="timeline-row__date ">  18 May  </div> </div>' +
            ' <div class="timeline-note "> ' +
            '<div class="js-area-loading timeline-note__loading" style="display: none"> ' +
            '<i class="icon-spin4 animate-spin loading-orb"></i> Loading... </div> ' +
            '<div class="js-area-content" style="display: none;">' +
            ' <div class="timeline-note__top"> <span class="timeline-tag">Note</span>  ' +
            '<span class="timeline-note__date">  <i class="icon-clock"></i> Posted 18 May 2016   </span> ' +
            '<a class="js-edit-note timeline-note__button"> <i class="icon-pencil"></i> Edit </a>  ' +
            '<a class="js-quick-delete timeline-note__button text--light hint--left" data-hint="Delete blank note"> <i class="icon-trash"></i> </a>  ' +
            '</div> <div class="timeline-note__content note-content js-note-content "></div>' +
            ' <div class="js-see-more-cont timeline-note__see-more" style="display: none"> ' +
            '<a class="js-see-more">Show more <span></span></a> </div>' +
            ' <ul class="timeline-note__attachments js-attachments"></ul> </div> ' +
            '<div class="js-area-edit timeline-note__edit" style=""> <div class="timeline-note__edit-top"> ' +
            '<div class="timeline-note__edit-top-block"> <span class="hidden-mobile" style="margin-right: 5px;">Created</span>' +
            ' <div class="js-date-picker" style="display: inline-block; margin-right: 15px;"><div class="date-time-picker">' +
            ' <div class="hint--bottom hint--always hint--error" style="display: inline-block; vertical-align: top">' +
            '<input class="form__input measure-row__input-rec-date js-input-date" type="text" value="18 May 2016" placeholder="Date" style="width: 100px; border-top-right-radius: 0; border-bottom-right-radius: 0; border-right-width: 0;"></div>' +
            '<a class="js-pickadate date-time-picker__cal-button picker__input" data-value="18 May 2016" id="P1884061833" type="text" aria-haspopup="true" aria-expanded="false" aria-readonly="false" aria-owns="P1884061833_root">' +
            '<i class="icon-calendar"></i></a>  ' +
            '</div></div> ' +
            '<div class="timeline-note__edit-pin-cont">' +
            ' <input type="checkbox" id="label-pin" class="js-edit-pinned">' +
            ' <label for="label-pin">Pin to top</label>' +
            ' <a class="help-prompt js-help-prompt-pin"><i class="icon-help-circled"></i></a> </div> </div> ' +
            '<div class="timeline-note__edit-top-block--right"> ' +
            '<input type="checkbox" id="label-archived" class="js-edit-archived">' +
            ' <label for="label-archived">Archived</label> </div> </div>' +
            ' <ul class="form timeline-note__edit-main-form"> <li class="form__row form__row--first">' +
            ' <a href="/help/guides/notes" target="_blank" style="float: right; position: relative; top: -3px">formatting help</a> ' +
            '<div class="form__row-heading-text">Content</div>' +
            ' <textarea class="js-input form__input timeline-note__input" style="margin-top: -1px; "></textarea> </li> ' +
            '<div class="js-help-popout-pin dropdown dropdown--help-snip help-snip" style="display: none;">' +
            ' <h3 class="help-snip__heading"> <i class="icon-pin"></i> Pinned notes </h3>' +
            ' <div class="help-snip__content"> ' +
            '<p>Notes are by default shown on the timeline in order of when they were first recorded, from newest to oldest. This is most relevant in long on-going goals or steps, where you can record notes relevant to a specific point in time, and look back on them with the important context of when they were written.</p> <p>On the other hand, some notes are always relevant (such as a list of links relating to a goal which you are regularly updating). You can <em>pin</em> notes to keep them stuck to the top of the timeline.</p> ' +
            '</div> </div> <li class="form__row" style="display: none"> ' +
            '<div class="form__row-heading-text">Add Attachments</div>' +
            '<div class="js-dropzone dropzone dz-clickable">' +
            '<div class="dz-default dz-message"><span>Drop files here to upload</span></div></div>' +
            ' </li> <li class="form__row form__row--last form__row--buttons" style="margin-top: 25px"> ' +
            '<a class="button button--cta js-save">Save</a>  <a class="button button--delete button--icon-left js-remove">' +
            '<i class="button__icon icon-trash"></i> Delete note</a> </li> </ul> </div> </div> </li>');
    });

    /* Add image  */
    $('.js-notes .js-compose-prompt[data-type="image"]').on('click', function(){
        $(this).parents('.js-add-buttons').hide();
    $(this).parents('.timeline-container__inner').children('ul').prepend('' +
        '<li class="timeline-row"> ' +
        '   <div class="timeline-row__mark"> ' +
        '       <div class="timeline-row__bubble"> <i class="icon-plus"></i> ' +
        '       </div> <div class="timeline-row__date ">  18 May  ' +
        '   </div> ' +
        '</div> ' +
        '<div class="timeline-note "> ' +
        '   <div class="js-area-loading timeline-note__loading" style="display: none"> ' +
        '       <i class="icon-spin4 animate-spin loading-orb"></i> Loading... ' +
        '   </div> ' +
        '   <div class="js-area-content" style="display: none;"> ' +
        '       <div class="timeline-note__top"> ' +
        '           <span class="timeline-tag">Note</span> ' +
        '           <span class="timeline-note__date"> <i class="icon-clock"></i> Posted 18 May 2016   </span> ' +
        '           <a class="js-edit-note timeline-note__button"> <i class="icon-pencil"></i> Edit </a>  ' +
        '           <a class="js-quick-delete timeline-note__button text--light hint--left" data-hint="Delete blank note"> <i class="icon-trash"></i> </a>  ' +
        '       </div> ' +
        '       <div class="timeline-note__content note-content js-note-content "></div> ' +
        '       <div class="js-see-more-cont timeline-note__see-more" style="display: none"> ' +
        '            <a class="js-see-more">Show more <span></span></a> ' +
        '       </div> ' +
        '       <ul class="timeline-note__attachments js-attachments"></ul> ' +
        '   </div> ' +
        '   <div class="js-area-edit timeline-note__edit" style="">' +
        '       <div class="timeline-note__edit-top"> ' +
        '            <div class="timeline-note__edit-top-block">' +
        '               <span class="hidden-mobile" style="margin-right: 5px;">Created</span>' +
        '               <div class="js-date-picker" style="display: inline-block; margin-right: 15px;">' +
        '               <div class="date-time-picker"> ' +
        '                    <div class="hint--bottom hint--always hint--error" style="display: inline-block; vertical-align: top">' +
        '                       <input class="form__input measure-row__input-rec-date js-input-date" type="text" value="18 May 2016" placeholder="Date" style="width: 100px; border-top-right-radius: 0; border-bottom-right-radius: 0; border-right-width: 0;">' +
        '                   </div>' +
        '                   <a class="js-pickadate date-time-picker__cal-button picker__input" data-value="18 May 2016" id="P1354568199" type="text" aria-haspopup="true" aria-expanded="false" aria-readonly="false" aria-owns="P1354568199_root">' +
        '                       <i class="icon-calendar"></i>' +
        '                       </a>  ' +
        '               </div>' +
        '           </div> ' +
        '           <div class="timeline-note__edit-pin-cont"> ' +
        '               <input type="checkbox" id="label-pin" class="js-edit-pinned"> ' +
        '               <label for="label-pin">Pin to top</label>' +
        '               <a class="help-prompt js-help-prompt-pin"><i class="icon-help-circled"></i></a> ' +
        '           </div> ' +
        '       </div>' +
        '       <div class="timeline-note__edit-top-block--right"> <input type="checkbox" id="label-archived" class="js-edit-archived"> ' +
        '           <label for="label-archived">Archived</label> ' +
        '       </div> ' +
        '   </div> ' +
        '   <ul class="form timeline-note__edit-main-form">' +
        '       <li class="form__row form__row--first"> ' +
        '           <a href="/help/guides/notes" target="_blank" style="float: right; position: relative; top: -3px">formatting help</a> ' +
        '           <div class="form__row-heading-text">Content</div> ' +
        '           <textarea class="js-input form__input timeline-note__input" style="margin-top: -1px; height: 70px"></textarea> ' +
        '       </li>' +
        '       <div class="js-help-popout-pin dropdown dropdown--help-snip help-snip" style="display: none;"> <h3 class="help-snip__heading">' +
        '           <i class="icon-pin"></i> Pinned notes </h3>' +
        '           <div class="help-snip__content"> ' +
        '               <p>Notes are by default shown on the timeline in order of when they were first recorded, from newest to oldest. This is most relevant in long on-going goals or steps, where you can record notes relevant to a specific point in time, and look back on them with the important context of when they were written.</p> <p>On the other hand, some notes are always relevant (such as a list of links relating to a goal which you are regularly updating). You can <em>pin</em> notes to keep them stuck to the top of the timeline.</p> ' +
        '           </div> ' +
        '       </div>' +
        '       <li class="form__row" style="">' +
        '           <div class="form__row-heading-text">Add Attachments</div>' +
        //'           <form action="#" class="dropzone needsclick dz-clickable" id="demo-upload">' +
        '               <div id="dropzone" class="js-dropzone dropzone dz-clickable">' +
        '                   <div class="dz-default dz-message">' +
        '                       <span>Drop files here to upload</span>' +
        '                   </div>' +
        '               </div> ' +
        //'           </form>' +
        '       <input type="file" multiple="multiple" class="dz-hidden-input" style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;">' +
        '       </li>' +
        '       <li class="form__row form__row--last form__row--buttons" style="margin-top: 25px"> ' +
        '           <a class="button button--cta js-save">Save</a>  ' +
        '           <a class="button button--delete button--icon-left js-remove"><i class="button__icon icon-trash"></i> Delete note</a> ' +
        '       </li> ' +
        '   </ul>' +
        '       </div> ' +
        '   </div> ' +
        '</li>');
        if ($('#dropzone').length) {
            $("div#dropzone").dropzone({ url: "auth/example" });
        };
    });

    /* ? click */
    $('.nodes_page .icon-help-circled').on('click', function (e) {
        e.stopPropagation();
        var left = $(this).offset().left - $('.js-live-bg-exclusion').offset().left +25;
        $(this).parents('.padded-content').find('.dropdown--help-snip').css({ top: '187px', left: left});
        $(this).parents('.padded-content').find('.dropdown--help-snip').toggle();
    });

    /* edit */
    $('.node-detail-buttons .js-start-edit').on('click', function(){
        $('.node-detail__main').html('<div class="padded-content" style="min-height: 13px"> ' +
            '<div style="margin-bottom: 10px;"> ' +
            '<i class="emblem emblem--small bg--inbox icon-inbox"></i>' +
            ' <a class="subtle-link" href="#nodes/68490">Inbox</a> � </div>  ' +
            '</div>' +
            '<div class="node-detail__conditional-padding"> ' +
            '<ul class="form form--boxed"> ' +
            '   <li class="form__row form__row--first"> ' +
            '   <label class="form__label">Step name</label> ' +
            '   <div style="display: table; width: 100%">' +
            '   <div class="js-name-prefix form__input-prefix hint--bottom"><i class="emblem bg--step icon-flag"></i></div>' +
            '<input type="text" class="form__input form__input--prefixed js-edit-name" placeholder="Name cant be left blank" value="^" style="max-width: 560px"></div> </li>   ' +
            ' <li class="form__row"> <div class="form__row-heading"> <span class="form__row-heading-text">Step type</span> ' +
            '<a class="help-prompt js-help-prompt-repeat"><i class="icon-help-circled"></i></a> </div> </li> ' +
            '<li class="form__row form__row--space-bottom"> <ul class="choice-box js-choice-repeating">' +
            '<li class="choice-box__item" data-choice="null">One-off</li><li class="choice-box__item" data-choice="periodic">Every <i>X</i> days</li>' +
            '<li class="choice-box__item" data-choice="weekly_schedule">Weekly schedule</li>' +
            '<li class="choice-box__item" data-choice="habit">Habit</li>' +
            '<li class="choice-box__item choice-box__item--selected" data-choice="sporadic">Sporadic</li></ul>' +
            ' <div class="js-help-popout-repeat dropdown dropdown--help-snip help-snip" style="display: none;"> ' +
            '<h3 class="help-snip__heading"> <i class="icon-cw"></i> Repeating steps </h3> ' +
            '<div class="help-snip__content"> <p>There are a few types of step, which behave in different ways on your to-do list:</p>' +
            ' <ul> <li><strong>One-off</strong> - When you complete a one-off step, it will become archived, and disappear off all active lists at midnight on the day of completion.</li> ' +
            '<li><strong>Every <em>X</em> days</strong> - Every time you complete the step, its due date will be pushed into the future by the number of days you specify. E.g. "every 1 day" for daily, "every 2 days" for alternate days, or "every 7 days" for once a week.</li> <li><strong>Weekly schedule</strong> - Specify which days of the week you want the step to fall on, and when its completed, it will move to the closest selected day.</li> ' +
            '<li><strong>Habit</strong> - This is for steps which you want to track multiple times per day. The habits they track can either be positive (like doing exercise), or negative (like smoking). They have no due dates, and always appear at the top of your to-do list until theyre archived.</li> <li><strong>Sporadic</strong> - When you complete the step, the due date will get reset to nothing, but the step <em>wont</em> get archived, so youll be able to give it a new due date, or complete it again at any time in the future.</li> </ul> <p>Learn more in the <a href="/help/guides/repeating" target="_blank">repeating steps feature guide</a>.</p> </div> </div> </li> <li class="form__row js-rowvis-periodic" style="display: none;"> <span class="js-repeats-detail"> Repeat every <input class="form__input js-edit-frequency" value="0" style="width: 30px;"> days </span> </li> <li class="form__row js-rowvis-periodic" style="display: none;"> <input type="checkbox" class="js-edit-check-skip-weekends"> Skip weekends <a class="help-prompt js-help-prompt-weekends"><i class="icon-help-circled"></i></a> <div class="js-help-popout-weekends dropdown dropdown--help-snip help-snip" style="display: none;"> <div class="help-snip__content"> <p>With <strong>skip weekends</strong> enabled, repeating steps will only consider weekdays. This is useful with tasks relating to a Monday-Friday work schedule. For example, if you complete a step that repeats every 2 days on a Friday, the next day it will fall on will be a Tuesday, instead of a Sunday.</p> </div> </div> </li> <li class="form__row js-rowvis-periodic" style="display: none;"> <input type="checkbox" class="js-edit-check-periodic-push-missed"> When a day is missed, update the due date <a class="help-prompt js-help-prompt-pushp"><i class="icon-help-circled"></i></a> </li> <div class="js-help-popout-pushp dropdown dropdown--help-snip help-snip" style="display: none;"> <div class="help-snip__content"> <p>Its usually best to leave this option unticked, and allow steps which you miss to fall into the "overdue" section of your To-do list.</p> <p>However, if you really want to keep a tight schedule, you can enable this option to keep the due date updated no matter what happens.</p> <p><em>Warning: This will cause you to continue receiving notifications even if you stop updating your to-do list on the app.</em></p> </div> </div> <li class="form__row js-rowvis-schedule" style="display: none;"> Days of the week <ul class="choice-box js-choice-weekly-schedule"><li class="choice-box__item">M</li><li class="choice-box__item">T</li><li class="choice-box__item">W</li><li class="choice-box__item">T</li><li class="choice-box__item">F</li><li class="choice-box__item">S</li><li class="choice-box__item">S</li></ul> </li> <li class="form__row js-rowvis-schedule" style="display: none;"> <input type="checkbox" class="js-edit-check-schedule-push-missed"> When missed, update the due date <a class="help-prompt js-help-prompt-pushw"><i class="icon-help-circled"></i></a> </li> <div class="js-help-popout-pushw dropdown dropdown--help-snip help-snip" style="display: none;"> <div class="help-snip__content"> <p>Its usually best to leave this option unticked, and allow steps which you miss to fall into the "overdue" section of your To-do list.</p> <p>However, if you really want to keep a tight schedule, you can enable this option to keep the due date updated no matter what happens.</p> <p><em>Warning: This will cause you to continue receiving notifications even if you stop updating your to-do list on the app.</em></p> </div> </div> <li class="form__row js-rowvis-habit" style="display: none;"> <label class="form__label">Possible actions</label> <select class="js-edit-hidden-action-button form__input"> <option value="null">Positive or negative ( + / - )</option> <option value="failed">Positive only ( + )</option> <option value="completed">Negative only ( - )</option> </select> </li> <li class="form__row js-rowhid-habit"> <div class="form__row-heading"> <span class="form__row-heading-text">Next due</span> <a class="help-prompt js-help-prompt-due"><i class="icon-help-circled"></i></a> <span class="hidden-mobile"> (<a class="js-clear-due">clear</a>) </span> </div> <div class="js-help-popout-due dropdown dropdown--help-snip help-snip" style="display: none;"> <h3 class="help-snip__heading"> Due dates </h3> <div class="help-snip__content"> <p>Steps can optionally have a <em>next due</em> date, so that they show in the to-do list and calendar. Either use the calendar widget to pick a date, or type one in the format DD MMM YY (e.g. 5 Nov 13).</p> <p><em>Next due</em> can be left as just a date, but to take advantage of email reminders, youll need to provide a time of day in the second box (so that we know when to send the reminder). Times can be any sensible format (18:00, 6:00pm, 6pm etc).</p> </div> </div> </li> <li class="form__row js-rowhid-habit"> <div class="js-date-time-picker"><div class="date-time-picker"> <div class="hint--bottom hint--always hint--error" style="display: inline-block; vertical-align: top"><input class="form__input measure-row__input-rec-date js-input-date" type="text" value="26 May 2016" placeholder="Date" style="width: 100px; border-top-right-radius: 0; border-bottom-right-radius: 0; border-right-width: 0;"></div><a class="js-pickadate date-time-picker__cal-button picker__input" data-value="26 May 2016" id="P1588057078" type="text" aria-haspopup="true" aria-expanded="false" aria-readonly="false" aria-owns="P1588057078_root"><i class="icon-calendar"></i></a>' +
            ' <div style="display: inline-block; margin: 0 5px;"> at </div>  <div style="display: inline-block;"> <input class="form__input js-input-time" type="text" placeholder="Time" value="" style="width: 50px;">  </div>  </div></div> </li> <li class="form__row js-rowhid-habit"> <div class="form__row-heading"> <span class="form__row-heading-text">Reminders</span> <a class="help-prompt js-help-prompt-reminders"><i class="icon-help-circled"></i></a> </div> <div class="js-help-popout-reminders dropdown dropdown--help-snip help-snip" style="display: none;"> <h3 class="help-snip__heading"> Reminders </h3> <div class="help-snip__content"> <p>Email and SMS (if youve provided a mobile number in the Profile tab) reminders can be scheduled for any step which has a due date <em>and</em> a due time.</p> <p>If a step isnt marked as completed and falls into the overdue list, <em>overdue reminders</em> will be sent for up to 2 days following, but after that no more reminders will send. To re-enable reminders for an overdue step, <em>postpone</em> it, or update its due date, so it becomes current again.</p> </div> </div> </li> <li class="form__row js-rowhid-habit"> <input type="checkbox" class="js-edit-check-remind" disabled="disabled"> <i class="icon-mail"></i> Email me a reminder <input type="text" class="js-edit-remind form__input" style="width: 30px" value="15" disabled="disabled"> minutes prior <span class="js-edit-remind-notice" style="font-style: italic">(requires a due time!)</span> </li> <li class="form__row js-rowhid-habit"> <input type="checkbox" class="js-edit-check-sms-remind" disabled="disabled"> <i class="icon-mobile"></i> Send an SMS reminder <input type="text" class="js-edit-sms-remind form__input" style="width: 30px" value="5" disabled="disabled"> minutes prior <span class="js-edit-notice-sms-remind-mobile" style="font-style: italic; display: none;">(requires a mobile number)</span> <span class="js-edit-notice-sms-remind-due" style="font-style: italic">(requires a due time!)</span> </li>  <li class="form__row form__row--last form__row--buttons"> <a href="javascript:;" class="button button--cta js-save-edit">Save</a> <a href="javascript:;" class="button js-discard-edit">Cancel</a> <a class="button button--delete button--icon-left js-delete"><i class="button__icon icon-trash"></i> Delete step</a> </li> </ul> </div>');

    });

    $('.heading-toggle').on('click', function () {
        var _this = $(this);
        var height = _this.height();
        if( _this.next().is(":visible")){
            _this.next().animate({
                height: 0,
                overflow: 'hidden'
            }, 700, function(){
                _this.next().hide();
            })
        } else {
            _this.next().animate({
                height: height+'px',
            }, 700, function(){
                _this.next().show();
            })
        }

    })
});

function complete_to_do(_this){
    _this.parents('.todo').toggleClass('todo--done');
    _this.parents('.todo').find('.todo__box--check').find('i').toggleClass('js-status-icon icon-check');
    if(_this.parents('.todo').find('.js-button-todo-toggle').attr('data-value') == 'false'){
        _this.parents('.todo').find('.js-button-todo-toggle').html('<i class="dropdown__icon"></i> Complete to-do');
        _this.parents('.todo').find('.js-button-todo-toggle').attr('data-value','true');
        _this.parents('.todo').find('.js-button-expand').show();
    } else {
        _this.parents('.todo').find('.js-button-todo-toggle').html('<i class="dropdown__icon"></i> Uncomplete to-do');
        _this.parents('.todo').find('.js-button-todo-toggle').attr('data-value','false');
        _this.parents('.todo').find('.js-button-expand').hide();
    }
    $('.dropdown--shortcuts').hide();
}

function formatTime(time) {
    var result = '', m;
    var re = /^\s*([01]?\d|2[0-3])(:?([0-5]\d))?\s*$/;
    if ((m = time.match(re))) {
        result = (m[1].length == 2 ? "" : "0") + m[1] + ":" + (m[2] || "00");
    }
    return result;
}

function due_date(_this, date, selector){
    selector.show();
    $('.dropdown--shortcuts').css({'opacity':0, 'display':'none'});
    _this.parents('.todo').remove();
    var a = _this.parents('.todo').find('.todo__date').find('.todo__options').html();
    var span = '';
    if(_this.parents('.todo').find('.todo__date').find('.hint--left').html()) {
        span = _this.parents('.todo').find('.todo__date').find('.hint--left').html();
    }
    _this.parents('.todo').find('.todo__date').html('');
    _this.parents('.todo').find('.todo__date').prepend('<span class="hint--left">'+span+'</span>'+date+' <a class="todo__options">'+a+'</a>');
    var todo = _this.parents('.todo').html();
    selector.find('.js-todo-children').append('<li class="todo" style="display: list-item;">'+todo+'</li>');
    selector.find('.dropdown--shortcuts').find('.js-expand-due').find('.dropdown__data').remove();
    if(date && date != ''){
        selector.find('.dropdown--shortcuts').find('.js-expand-due').prepend('<div class="dropdown__data"> '+date+' </div>' +
                                                                            '<a href="javascript:;" class="dropdown__button dropdown__button--nested js-button-due" data-target="none">' +
                                                                            '<i class="dropdown__icon"></i> ' +
                                                                            '<em>None</em> </a>');
    } else {
        selector.find('.dropdown--shortcuts').find("a[data-target='none']").hide();
    }
    selector.find('.dropdown--shortcuts').find("a[data-target='today']").show();
    if($.trim(date) == 'Today'){
        selector.find('.dropdown--shortcuts').find("a[data-target='today']").hide();
    } else if($.trim(date) == 'Tomorrow'){
        selector.find('.dropdown--shortcuts').find("a[data-target='tomorrow']").hide();
    }
    $('.todos').each(function() {
        if($.trim($(this).find('.js-todo-children').html()) == ''){
            $(this).hide();
        }
    });
};

/*  check phone number validation */
function validatePhone(txtPhone) {
    var filter =  /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
    if (filter.test(txtPhone)) {
        return true;
    }
    else {
        return false;
    }
};