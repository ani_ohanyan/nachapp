<?php
    use yii\helpers\Url;
?>
<div id="js-page-goals" class="t-region t-region--active" style="display: block;">
    <div>
        <div class="js-tab-strip" style="">
            <div class="tab-strip tab-strip--goals">
                <li class="js-tab tab-strip__tab--all tab-strip__tab tab-strip__tab--selected" data-tab="all">
                    <a href="javascript:;" class="tab-strip__anchor">All<span class="tab-strip__count">3</span></a>
                </li>
                <?php foreach($category as $data):?>
                    <li class="js-tab tab-strip__tab--cat tab-strip__tab  ui-droppable" data-tab="cat-3096">
                        <a href="#" class="tab-strip__anchor"><?=$data['name'];?>
                            <span class="tab-strip__count">1</span>
                        </a>
                    </li>
                <?php endforeach;?>
                <li class="js-tab tab-strip__tab--uncat tab-strip__tab  ui-droppable" data-tab="uncat">
                    <a  href="javascript:;" class="tab-strip__anchor">None<span class="tab-strip__count">3</span></a>
                </li>
                <li class="tab-strip__tab tab-strip__tab--edit">
                    <a class="tab-strip__anchor hint--right" href="<?= Url::home().'categories'?>" data-hint="Manage categories">
                        <i class="icon-pencil"></i>
                    </a>
                </li>
            </div>
        </div>
        <div class="js-main-container nodes-list">
            <div class="js-filters filters-exterior__in">
                <div class="filters">
                    <div class="js-area-prompt filters__area-prompt" style="display: none;">
                        <a class="js-prompt-load" style="padding: 8px 0;">Load filters</a>
                        <a class="help-prompt js-help-prompt-filters"><i class="icon-help-circled"></i></a>
                        <div class="js-help-popout-filters dropdown dropdown--help-snip help-snip" style="display: none;">
                            <div class="help-snip__content">
                                <p>As you have quite a few goals and steps, we've started you out with filters disabled and your goals collapsed. This makes the initial loading of the app considerably faster.</p>

                                <p>If you'd like to enable the filters, simply click "Load filters", but be aware that, ?specially on mobile, it may take a few seconds for everything to load.</p></div>
                        </div>
                    </div>
                    <div class="js-area-loading filters__area-loading" style="display: none;">
                        <span><i class="icon-spin4 animate-spin loading-orb"></i> Loading...</span>
                    </div>
                    <div class="js-area-live" style="display: block;">
                        <div style="display: table; width: 100%;">
                            <div style="display: table-cell; vertical-align: top">
                                <div class="filters__heading"><span>Filter by name</span>
                                    <a class="filters__clear js-clear-name" style="display: none"><i class="icon-cancel"></i></a>
                                </div>
                                <input name="filter-by-name" type="text" value="" class="filters__search-input js-filter-name">
                            </div>
                            <div class="filters__toggles-mobile"><a class="button js-collapse">?</a>
                                <a class="button js-expand">?</a>
                            </div>
                        </div>
                        <div class="filters__toggles-desktop">
                            <div class="filters__heading">Toggles</div>
                            <ul>
                                <li><a href="javascript:;" class="js-collapse"><i class="icon-up-dir"></i>Collapse
                                        all</a></li>
                                <li><a href="javascript:;" class="js-expand"><i class="icon-down-dir"></i>Expand all</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="js-filters-exterior filters-exterior__out">
                <p class="js-no-nodes" style="margin: 20px 25px; line-height: 1.9; display: none">
                    <i class="icon-info"></i> Welcome to Nach! If you're new here,
                    <a href="javascript:;" class="js-trigger-tutorial"  style="font-weight: 600">try our interactive tutorial</a>.
                </p>
                <ul class="js-list-inbox">
                    <li class="node-summary js-draggable-node js-node-id-68490" style="display: list-item;">
                        <div class="js-self node-summary__self" >
                            <a class="node-summary__toggle js-toggle icon-down-dir"></a>
                            <i class="node-summary__emblem emblem bg--inbox icon-inbox  js-emblem-menu"></i>
                            <div class="js-main node-summary__main">
                                <div class="node-summary__editable-name js-editable-name">
                                    <a href="#nodes/68490" class="node-summary__name js-name-value editable-stat" data-name="Inbox">Inbox</a>
                                </div>
                                <span class="js-icons node-summary__icons">
                                    <span class="js-category node-summary__category" style="display: none;">
                                        <i class="icon-folder"></i><span></span></span>
                                </span>
                            </div>
                        </div>
                        <div class="node-summary__children js-children">
                            <ul class="js-list-archived" style="display: none"></ul>
                            <ul class="js-list" data_id = '0'>
                                <?php foreach($inbox as $goals):?>
                                        <li class="node-summary js-draggable-node js-node-id-69261 ui-draggable" style="display: list-item;">
                                            <div class="js-self node-summary__self ui-droppable">
                                                <a class="node-summary__toggle js-toggle icon-right-dir" style="visibility: hidden"></a>
                                                <i class="node-summary__emblem emblem bg--step icon-flag js-sort-handle emblem--sort-handle js-emblem-menu"></i>
                                                <div class="js-main node-summary__main">
                                                    <div class="node-summary__editable-name js-editable-name">
                                                        <div class="node-summary__editable-name js-editable-name">
                                                            <div class="editable-stat editable-stat--node-summary">
                                                                <span class="js-area-read">
                                                                    <a class="node-summary__name js-name-value" href="<?=Url::to(['/nodes', 'id'=>$goals['id']])?>" data-name="<?=$goals['name'];?>"><?=$goals['name'];?></a>
                                                                    <a class="editable-stat__button editable-stat__edit-prompt js-stat-edit hint--right" data-hint="Edit">
                                                                        <i class="icon-pencil"></i>
                                                                    </a>
                                                                </span>
                                                                <span class="js-area-edit" style="display: none">
                                                                    <span class="js-error hint--right hint--always hint--error">
                                                                        <input name="behat-editable-stat-input" type="text" class="editable-stat__input js-stat-input behat-editable-stat-input" value="anasun\" maxlength="255">
                                                                    </span>
                                                                    <a class="editable-stat__button js-stat-save hint--right" data-hint="Save">
                                                                        <i class="icon-check"></i>
                                                                    </a>
                                                                    <a class="editable-stat__button js-stat-discard hint--right" data-hint="Cancel">
                                                                        <i class="icon-cancel"></i>
                                                                    </a>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <span class="js-icons node-summary__icons">
                                                        <span class="hint--right" data-hint="1 Note">
                                                            <i class="icon-doc-alt"></i>
                                                        </span>
                                                        <span class="js-category node-summary__category" style="display: none;">
                                                            <i class="icon-folder"></i>
                                                            <span></span>
                                                        </span>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="dropdown dropdown--shortcuts dropdown--visible" >
                                                <div class="dropdown__item dropdown__item--heading">Shortcuts</div>
                                                <a class="dropdown__button js-button-rename">
                                                    <i class="dropdown__icon icon-pencil"></i> Rename
                                                </a>
                                                <a class="dropdown__button js-button-todo-toggle" data-status="completed" data-value="true">
                                                    <i class="dropdown__icon icon-check"></i> Complete to-do
                                                </a>
                                                <a class="dropdown__button js-button-expand" data-expand="due">
                                                    <i class="dropdown__icon"></i> Set next due...
                                                    <i class="dropdown__nest-indicator icon-down-dir"></i>
                                                </a>
                                                <div class="js-expand-due" style="display: none;">
                                                    <a class="dropdown__button dropdown__button--nested js-button-due" data-target="today">
                                                        <i class="dropdown__icon"></i> Today
                                                    </a>
                                                    <a class="dropdown__button dropdown__button--nested js-button-due" data-target="tomorrow">
                                                        <i class="dropdown__icon"></i> Tomorrow
                                                    </a>
                                                    <a class="dropdown__button dropdown__button--nested js-button-custom-date" data-postpone="no">
                                                        <i class="dropdown__icon"></i> Custom...
                                                    </a>
                                                </div>
                                                <a class="dropdown__button js-button-delete">
                                                    <i class="dropdown__icon"></i>
                                                    <span class="text--delete">Delete</span>
                                                </a>
                                            </div>
                                            <div class="dropdown dropdown--shortcuts dropdown--visible" style="display: none;">
                                                <div class="dropdown__item dropdown__item--heading">Shortcuts</div>
                                                <a href="javascript:;" class="dropdown__button js-button-rename">
                                                    <i class="dropdown__icon icon-pencil"></i> Rename
                                                </a>
                                                <a href="javascript:;" class="dropdown__button js-button-goal-status-toggle"  data-status="completed" data-value="true">
                                                    <i class="dropdown__icon icon-check"></i> Complete goal
                                                </a>
                                                <a href="javascript:;" class="dropdown__button js-button-pause-toggle" data-pause="pause">
                                                    <i class="dropdown__icon icon-pause"></i> Pause goal </a>
                                                <a href="javascript:;" class="dropdown__button js-button-delete">
                                                    <i class="dropdown__icon"></i> <span class="text--delete">Delete</span>
                                                </a>
                                            </div>
                                        </li>
                                <?php endforeach;?>
                            </ul>
                            <div class="node-summary__add js-add">
                                <div class="node-add node-add--summary ui-droppable">
                                    <a class="node-summary__toggle icon-right-dir" style="visibility: hidden"></a>
                                    <div class="js-area-type-prompts node-add__area-type-prompts">
                                        <i class="js-prompt-emblem emblem emblem--plus node-summary__emblem icon-plus node-add__prompt-emblem"></i>
                                        <a href="javascript:;">Add step</a></div>
                                    <div class="js-area-input node-add__area-input" style="display: none">
                                        <div style="display: table; border-collapse: collapse; width: 100%">
                                            <div style="display: table-row">
                                                <div style="display: table-cell; width: 32px">
                                                    <i class="js-input-emblem emblem bg--goal icon-globe node-add__input-emblem" style="cursor: pointer"></i>
                                                </div>
                                                <div style="display: table-cell">
                                                    <input class="node-add__input js-input behat-node-add-input" value="" maxlength="255">
                                                </div>
                                                <a style="display: table-cell; width: 32px; text-align: center;" data-hint="Save" class="js-save-button hint--bottom">
                                                    <i class="icon-check"></i></a>
                                            </div>
                                        </div>
                                        <a class="node-add__show-parts js-show-parts" style="display: none">
                                            <i  class="icon-down-open"></i> Show shortcuts </a>
                                        <div class="js-parts" style="display: none">
                                            <ul class="node-add__parts">
                                                <li class="js-part js-part-due node-add__part node-add__part--due hint--bottom" data-shortcut="^">
                                                    <i class="icon-calendar"></i> <span></span>
                                                </li>
                                                <li class="js-part js-part-repeat node-add__part node-add__part--repeat" data-shortcut="*">
                                                    <i class="icon-cw"></i> <span></span>
                                                </li>
                                                <li class="js-part js-part-reminder node-add__part node-add__part--reminder hint--bottom" data-shortcut="!">
                                                    <i class="icon-mail"></i> <span></span>
                                                </li>
                                            </ul>
                                            <a class="node-add__parts-help" href="/help/guides/add-shortcuts" target="_blank">Shortcut reference &#187;</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <ul id="js-goals" class="top-level-goals js-list">
                    <?php foreach($goals as $goal):?>
                        <li class="node-summary js-draggable-node js-node-id-69277 ui-draggable" style="display: list-item;">
                            <div class="js-self node-summary__self ui-droppable">
                                <a class="node-summary__toggle js-toggle icon-down-dir"></a>
                                <i class="node-summary__emblem emblem bg--goal icon-globe js-sort-handle emblem--sort-handle js-emblem-menu"></i>
                                <div class="js-main node-summary__main">

                                    <div class="node-summary__editable-name js-editable-name">
                                        <div class="editable-stat editable-stat--node-summary">
                                            <span class="js-area-read">
                                                <a class="node-summary__name js-name-value" href="#nodes/70785" data-name="tt">tt</a>
                                                <a  class="editable-stat__button editable-stat__edit-prompt js-stat-edit hint--right" data-hint="Edit">
                                                    <i  class="icon-pencil"></i>
                                                </a>
                                            </span>
                                            <span class="js-area-edit" style=" display: none;">
                                                <span class="js-error hint--right hint--always hint--error">
                                                    <input name="behat-editable-stat-input" type="text" class="editable-stat__input js-stat-input behat-editable-stat-input" value="tt" maxlength="255"> </span>
                                                <a class="editable-stat__button js-stat-save hint--right" data-hint="Save">
                                                    <i class="icon-check"></i>
                                                </a>
                                                <a  class="editable-stat__button js-stat-discard hint--right" data-hint="Cancel">
                                                    <i class="icon-cancel"></i>
                                                </a>
                                            </span>
                                        </div>
                                    </div>

                                    <span class="js-icons node-summary__icons">
                                        <span class="js-category node-summary__category" style="display: none;">
                                            <i class="icon-folder"></i><span></span>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="node-summary__children js-children">
                                <ul class="js-list-archived" style="display: none"></ul>
                                <ul class="js-list">
                                    <li class="node-summary js-draggable-node js-node-id-69278 ui-draggable"
                                        style="display: list-item;">
                                        <div class="js-self node-summary__self ui-droppable">
                                            <a class="node-summary__toggle js-toggle icon-down-dir"></a>
                                            <i class="node-summary__emblem emblem bg--goal icon-globe js-sort-handle emblem--sort-handle js-emblem-menu"></i>
                                            <div class="js-main node-summary__main">
                                                <div class="node-summary__editable-name js-editable-name">
                                                    <div class="editable-stat editable-stat--node-summary">
                                                        <span class="js-area-read">
                                                            <a class="node-summary__name js-name-value" href="#nodes/70785" data-name="tt">tt</a>
                                                            <a  class="editable-stat__button editable-stat__edit-prompt js-stat-edit hint--right" data-hint="Edit">
                                                                <i  class="icon-pencil"></i>
                                                            </a>
                                                        </span>
                                                        <span class="js-area-edit" style=" display: none; ">
                                                            <span class="js-error hint--right hint--always hint--error">
                                                                <input name="behat-editable-stat-input" type="text" class="editable-stat__input js-stat-input behat-editable-stat-input" value="tt" maxlength="255"> </span>
                                                            <a class="editable-stat__button js-stat-save hint--right" data-hint="Save">
                                                                <i class="icon-check"></i>
                                                            </a>
                                                            <a  class="editable-stat__button js-stat-discard hint--right" data-hint="Cancel">
                                                                <i class="icon-cancel"></i>
                                                            </a>
                                                        </span>
                                                    </div>
                                                </div>
                                                <span class="js-icons node-summary__icons">
                                                    <span class="js-category node-summary__category" style="display: none;">
                                                        <i class="icon-folder"></i><span></span>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="node-summary__children js-children">
                                            <ul class="js-list-archived" style="display: none"></ul>
                                            <ul class="js-list">
                                                <li class="node-summary js-draggable-node js-node-id-70072 ui-draggable" style="display: list-item;">
                                                    <div class="js-self node-summary__self ui-droppable">
                                                        <a class="node-summary__toggle js-toggle icon-right-dir" style="visibility: hidden"></a>
                                                        <i class="node-summary__emblem emblem bg--step icon-flag js-sort-handle emblem--sort-handle js-emblem-menu"></i>
                                                        <div class="js-main node-summary__main">
                                                            <div class="node-summary__editable-name js-editable-name">
                                                                <div class="node-summary__editable-name js-editable-name">
                                                                    <div class="editable-stat editable-stat--node-summary">
                                                                        <span class="js-area-read">
                                                                            <a class="node-summary__name js-name-value" href="#" data-name="anasun\">anasun\</a>
                                                                            <a class="editable-stat__button editable-stat__edit-prompt js-stat-edit hint--right" data-hint="Edit">
                                                                                <i class="icon-pencil"></i>
                                                                            </a>
                                                                        </span>
                                                                        <span class="js-area-edit" style="display: none">
                                                                            <span class="js-error hint--right hint--always hint--error">
                                                                                <input name="behat-editable-stat-input" type="text" class="editable-stat__input js-stat-input behat-editable-stat-input" value="anasun\" maxlength="255">
                                                                            </span>
                                                                            <a class="editable-stat__button js-stat-save hint--right" data-hint="Save">
                                                                                <i class="icon-check"></i>
                                                                            </a>
                                                                            <a class="editable-stat__button js-stat-discard hint--right" data-hint="Cancel">
                                                                                <i class="icon-cancel"></i>
                                                                            </a>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <span class="js-icons node-summary__icons">
                                                                <span class="hint--right" data-hint="1 Note">
                                                                    <i class="icon-doc-alt"></i>
                                                                </span>
                                                                <span class="js-category node-summary__category" style="display: none;">
                                                                    <i class="icon-folder"></i>
                                                                    <span></span>
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="dropdown dropdown--shortcuts dropdown--visible" >
                                                        <div class="dropdown__item dropdown__item--heading">Shortcuts</div>
                                                        <a class="dropdown__button js-button-rename">
                                                            <i class="dropdown__icon icon-pencil"></i> Rename
                                                        </a>
                                                        <a class="dropdown__button js-button-todo-toggle" data-status="completed" data-value="true">
                                                            <i class="dropdown__icon icon-check"></i> Complete to-do
                                                        </a>
                                                        <a class="dropdown__button js-button-expand" data-expand="due">
                                                            <i class="dropdown__icon"></i> Set next due...
                                                            <i class="dropdown__nest-indicator icon-down-dir"></i>
                                                        </a>
                                                        <div class="js-expand-due" style="display: none;">
                                                            <a class="dropdown__button dropdown__button--nested js-button-due" data-target="today">
                                                                <i class="dropdown__icon"></i> Today
                                                            </a>
                                                            <a class="dropdown__button dropdown__button--nested js-button-due" data-target="tomorrow">
                                                                <i class="dropdown__icon"></i> Tomorrow
                                                            </a>
                                                            <a class="dropdown__button dropdown__button--nested js-button-custom-date" data-postpone="no">
                                                                <i class="dropdown__icon"></i> Custom...
                                                            </a>
                                                        </div>
                                                        <a class="dropdown__button js-button-delete">
                                                            <i class="dropdown__icon"></i>
                                                            <span class="text--delete">Delete</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                            <div class="node-summary__add js-add">
                                                <div class="node-add node-add--summary ui-droppable">
                                                    <a class="node-summary__toggle icon-right-dir" style="visibility: hidden"></a>
                                                    <div class="js-area-type-prompts node-add__area-type-prompts">
                                                        <i class="js-prompt-emblem emblem emblem--plus node-summary__emblem icon-plus node-add__prompt-emblem"></i>
                                                        <a href="javascript:;">Add step</a>
                                                        <span style="opacity: 0.3; margin: 0 3px">|</span>
                                                        <a href="javascript:;">Add sub-goal</a>
                                                    </div>
                                                    <div class="js-area-input node-add__area-input" style="display: none">
                                                        <div style="display: table; border-collapse: collapse; width: 100%">
                                                            <div style="display: table-row">
                                                                <div style="display: table-cell; width: 32px">
                                                                    <i class="js-input-emblem emblem bg--goal icon-globe node-add__input-emblem"  style="cursor: pointer"></i>
                                                                </div>
                                                                <div style="display: table-cell">
                                                                    <input class="node-add__input js-input behat-node-add-input" value="" maxlength="255">
                                                                </div>
                                                                <a style="display: table-cell; width: 32px; text-align: center;" data-hint="Save" class="js-save-button hint--bottom">
                                                                    <i class="icon-check"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <a class="node-add__show-parts js-show-parts" style="display: none">
                                                            <i class="icon-down-open"></i> Show shortcuts </a>
                                                        <div class="js-parts" style="display: none">
                                                            <ul class="node-add__parts">
                                                                <li class="js-part js-part-due node-add__part node-add__part--due hint--bottom" data-shortcut="^">
                                                                    <i class="icon-calendar"></i>
                                                                    <span></span>
                                                                </li>
                                                                <li class="js-part js-part-repeat node-add__part node-add__part--repeat"
                                                                    data-shortcut="*"><i class="icon-cw"></i> <span></span>
                                                                </li>
                                                                <li class="js-part js-part-reminder node-add__part node-add__part--reminder hint--bottom"
                                                                    data-shortcut="!"><i class="icon-mail"></i>
                                                                    <span></span>
                                                                </li>
                                                            </ul>
                                                            <a class="node-add__parts-help" href="/help/guides/add-shortcuts" target="_blank">Shortcut reference �</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dropdown dropdown--shortcuts dropdown--visible" style="display: none;">
                                            <div class="dropdown__item dropdown__item--heading">Shortcuts</div>
                                            <a href="javascript:;" class="dropdown__button js-button-rename">
                                                <i class="dropdown__icon icon-pencil"></i> Rename
                                            </a>
                                            <a href="javascript:;" class="dropdown__button js-button-goal-status-toggle"  data-status="completed" data-value="true">
                                                <i class="dropdown__icon icon-check"></i> Complete goal
                                            </a>
                                            <a href="javascript:;" class="dropdown__button js-button-pause-toggle" data-pause="pause">
                                                <i class="dropdown__icon icon-pause"></i> Pause goal </a>
                                            <a href="javascript:;" class="dropdown__button js-button-delete">
                                                <i class="dropdown__icon"></i> <span class="text--delete">Delete</span>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                                <div class="node-summary__add js-add">
                                    <div class="node-add node-add--summary ui-droppable">
                                        <a class="node-summary__toggle icon-right-dir" style="visibility: hidden"></a>
                                        <div class="js-area-type-prompts node-add__area-type-prompts">
                                            <i  class="js-prompt-emblem emblem emblem--plus node-summary__emblem icon-plus node-add__prompt-emblem"></i>
                                            <a href="javascript:;">Add step</a>
                                            <span style="opacity: 0.3; margin: 0 3px">|</span> <a href="javascript:;">Add sub-goal</a>
                                        </div>
                                        <div class="js-area-input node-add__area-input" style="display: none">
                                            <div style="display: table; border-collapse: collapse; width: 100%">
                                                <div style="display: table-row">
                                                    <div style="display: table-cell; width: 32px">
                                                        <i class="js-input-emblem emblem bg--goal icon-globe node-add__input-emblem"   style="cursor: pointer"></i></div>
                                                    <div style="display: table-cell">
                                                        <input class="node-add__input js-input behat-node-add-input" value="" maxlength="255">
                                                    </div>
                                                    <a style="display: table-cell; width: 32px; text-align: center;"
                                                       data-hint="Save" class="js-save-button hint--bottom">
                                                        <i class="icon-check"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <a class="node-add__show-parts js-show-parts" style="display: none">
                                                <i class="icon-down-open"></i> Show shortcuts </a>
                                            <div class="js-parts" style="display: none">
                                                <ul class="node-add__parts">
                                                    <li class="js-part js-part-due node-add__part node-add__part--due hint--bottom"  data-shortcut="^">
                                                        <i class="icon-calendar"></i> <span></span>
                                                    </li>
                                                    <li class="js-part js-part-repeat node-add__part node-add__part--repeat"  data-shortcut="*">
                                                        <i class="icon-cw"></i> <span></span>
                                                    </li>
                                                    <li class="js-part js-part-reminder node-add__part node-add__part--reminder hint--bottom" data-shortcut="!">
                                                        <i class="icon-mail"></i> <span></span>
                                                    </li>
                                                </ul>
                                                <a class="node-add__parts-help" href="/help/guides/add-shortcuts" target="_blank">Shortcut reference &#187; </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown dropdown--shortcuts dropdown--visible" style="display: none;">
                                <div class="dropdown__item dropdown__item--heading">Shortcuts</div>
                                <a href="javascript:;" class="dropdown__button js-button-rename">
                                    <i class="dropdown__icon icon-pencil"></i> Rename
                                </a>
                                <a href="javascript:;" class="dropdown__button js-button-goal-status-toggle"  data-status="completed" data-value="true">
                                    <i class="dropdown__icon icon-check"></i> Complete goal
                                </a>
                                <a href="javascript:;" class="dropdown__button js-button-pause-toggle" data-pause="pause">
                                    <i class="dropdown__icon icon-pause"></i> Pause goal </a>
                                <a href="javascript:;" class="dropdown__button js-button-delete">
                                    <i class="dropdown__icon"></i> <span class="text--delete">Delete</span>
                                </a>
                            </div>
                        </li>
                    <?php endforeach;?>
                </ul>
                <div class="node-summary__add js-add">
                    <div class="node-add node-add--summary node-summary-add--top-level ui-droppable">
                        <a class="node-summary__toggle icon-right-dir" style="visibility: hidden"></a>
                        <div class="js-area-type-prompts node-add__area-type-prompts">
                            <i class="js-prompt-emblem emblem emblem--plus node-summary__emblem icon-plus node-add__prompt-emblem"></i>
                            <a href="javascript:;">Create new goal</a></div>
                        <div class="js-area-input node-add__area-input" style="display: none">
                            <div style="display: table; border-collapse: collapse; width: 100%">
                                <div style="display: table-row">
                                    <div style="display: table-cell; width: 32px">
                                        <i class="js-input-emblem emblem bg--goal icon-globe node-add__input-emblem" style=""></i>
                                    </div>
                                    <div style="display: table-cell">
                                        <input class="node-add__input js-input behat-node-add-input" value=""
                                            maxlength="255"></div>
                                    <a style="display: table-cell; width: 32px; text-align: center;" data-hint="Save"
                                       class="js-save-button hint--bottom"><i class="icon-check"></i></a>
                                </div>
                            </div>
                            <a class="node-add__show-parts js-show-parts" style="display: none">
                                <i class="icon-down-open"></i> Show shortcuts
                            </a>
                            <div class="js-parts" style="display: none">
                                <ul class="node-add__parts">
                                    <li class="js-part js-part-due node-add__part node-add__part--due hint--bottom" data-shortcut="^">
                                        <i class="icon-calendar"></i> <span></span>
                                    </li>
                                    <li class="js-part js-part-repeat node-add__part node-add__part--repeat" data-shortcut="*">
                                        <i class="icon-cw"></i> <span></span>
                                    </li>
                                    <li class="js-part js-part-reminder node-add__part node-add__part--reminder hint--bottom" data-shortcut="!">
                                        <i class="icon-mail"></i> <span></span>
                                    </li>
                                </ul>
                                <a class="node-add__parts-help" href="/help/guides/add-shortcuts" target="_blank">Shortcut reference &#187;</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>