<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'To Do';
?>

<div id="js-page-todos" class="t-region t-region--active" style="display: block;">
    <div>
            <div class="js-tab-strip" style="display: block;">
                <div class="tab-strip">
                    <li class="js-tab tab-strip__tab--all tab-strip__tab tab-strip__tab--selected" data-tab="all">
                        <a href="javascript:;" class="tab-strip__anchor">All
                            <span class="tab-strip__count">11</span>
                        </a>
                    </li>
                    <?php foreach($category as $data):?>
                        <li class="js-tab tab-strip__tab--cat tab-strip__tab  ui-droppable" data-tab="cat-3096">
                            <a href="javascript:;" class="tab-strip__anchor"><?=$data['name'];?>
                                <span class="tab-strip__count">1</span>
                            </a>
                        </li>
                    <?php endforeach;?>
                    <li class="js-tab tab-strip__tab--uncat tab-strip__tab  ui-droppable" data-tab="uncat">
                        <a  href="javascript:;" class="tab-strip__anchor">None<span class="tab-strip__count">3</span></a>
                    </li>
                    <li class="tab-strip__tab tab-strip__tab--edit">
                        <a class="tab-strip__anchor hint--right" href="categories" data-hint="Manage categories">
                            <i class="icon-pencil"></i>
                        </a>
                    </li>
                </div>
            </div>

        <div class="js-empty-message padded-content" style="display: none; margin-top: 30px"> You don't have any steps set up yet. Visit the <a href="https://nachapp.com/app#goals">Goal Map</a> to start creating them
        </div>
        <div class="todos-quick-add"><a class="todos-icons__icon todos-icons__icon--print js-print hint--bottom" data-hint="Print" style="float: right"> <i class="icon-print"></i> </a>

            <div class="todos-quick-add__slot js-quick-add">
                <div class="node-add node-add--todos ui-droppable"><a class="node-summary__toggle icon-right-dir" style="visibility: hidden"></a>
                    <div class="js-area-type-prompts node-add__area-type-prompts"><a> <i class="icon-plus"></i><span>Create a new step</span> <span class="node-add__inbox-hint">Step will be placed in your Inbox</span>
                        </a></div>
                    <div class="js-area-input node-add__area-input" style="display: none">
                        <div style="display: table; border-collapse: collapse; width: 100%">
                            <div style="display: table-row">
                                <div style="display: table-cell"><input class="node-add__input js-input behat-node-add-input" value="" maxlength="255"></div>
                                <a style="display: table-cell; width: 32px; text-align: center;" data-hint="Save" class="js-save-button hint--bottom"><i class="icon-check"></i></a></div>
                        </div>
                        <a class="node-add__show-parts js-show-parts" style="display: none"> <i class="icon-down-open"></i> Show shortcuts </a>

                        <div class="js-parts" style="display: none">
                            <ul class="node-add__parts">
                                <li class="js-part js-part-due node-add__part node-add__part--due hint--bottom" data-shortcut="^"><i class="icon-calendar"></i> <span>^</span>
                                </li>
                                <li class="js-part js-part-repeat node-add__part node-add__part--repeat" data-shortcut="*"><i class="icon-cw"></i> <span>*</span></li>
                                <li class="js-part js-part-reminder node-add__part node-add__part--reminder hint--bottom" data-shortcut="!"><i class="icon-mail"></i> <span>!</span></li>
                            </ul>
                            <a class="node-add__parts-help" href="#" target="_blank">Shortcut reference &#187;</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="js-list-habit todos" style="display:none;">
            <div  >
                <div class="todos__divider todos__divider--habit"> Habits</div>
                <ul class="js-todo-children"></ul>
            </div>
        </div>
        <div class="js-list-overdue todos">
            <div style="display: block;">
                <div class="todos__divider todos__divider--overdue"> Overdue</div>
                <ul class="js-todo-children">
                    <li class="todo" style="display: list-item;">
                        <a class="todo__box todo__box--check "><i class="js-status-icon"></i></a>
                        <div class="todo__date">
                                                <span class="hint--left" data-hint="Repeat: sporadic">
                                                    <i class="icon-cw"></i>
                                                </span> Yesterday
                            <a class="todo__options"> <i class="icon-down-dir"></i> </a>
                        </div>
                        <div class="todo__desc"><a href="#nodes/69260" data-hint="Goal: Inbox" class="todo__name hint--right">^</a>
                            <div class="js-streak streak" data-heat="0" style="display: none">
                                <i class="icon-cw"></i><span></span>
                            </div>
                            <span class="todo__icons">   </span>
                        </div>
                        <div class="dropdown dropdown--shortcuts" style="display: none; opacity: 0;">
                            <div class="dropdown__item dropdown__item--heading">Shortcuts</div>
                            <a class="dropdown__button js-button-todo-toggle" data-status="completed" data-value="true">
                                <i class="dropdown__icon icon-check"></i> Complete to-do
                            </a>
                            <a class="dropdown__button js-button-expand" data-expand="due">
                                <i class="dropdown__icon"></i> Set next due... <i class="dropdown__nest-indicator icon-down-dir"></i>
                            </a>
                            <div class="js-expand-due" style="display: none;">
                                <a class="dropdown__button dropdown__button--nested js-button-due" data-target="today">
                                    <i class="dropdown__icon"></i> Today
                                </a>
                                <a class="dropdown__button dropdown__button--nested js-button-due" data-target="tomorrow">
                                    <i class="dropdown__icon"></i> Tomorrow
                                </a>
                                <a class="dropdown__button dropdown__button--nested js-button-custom-date" data-postpone="no">
                                    <i class="dropdown__icon"></i> Custom...
                                </a>
                            </div>
                            <a class="dropdown__button js-button-delete">
                                <i class="dropdown__icon"></i>
                                <span class="text--delete">Delete</span>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="js-list-today todos">
            <div style="display: block;">
                <div class="todos__divider todos__divider--today"> Today</div>
                <ul class="js-todo-children">
                    <li class="todo" style="display: list-item;">
                        <a class="todo__box todo__box--check "><i class="js-status-icon"></i></a>
                        <div class="todo__date"> Today <a class="todo__options">
                                <i class="icon-down-dir"></i> </a>
                        </div>
                        <div class="todo__desc">
                            <a href="#nodes/69276" data-hint="Goal: Inbox" class="todo__name hint--right">asd</a>
                            <div class="js-streak streak" data-heat="0" style="display: none">
                                <i class="icon-cw"></i>
                                <span></span>
                            </div>
                            <span class="todo__icons">   </span>
                        </div>
                        <div class="dropdown dropdown--shortcuts" style="display: none; opacity: 0;">
                            <div class="dropdown__item dropdown__item--heading">Shortcuts</div>
                            <a class="dropdown__button js-button-todo-toggle" data-status="completed" data-value="true">
                                <i class="dropdown__icon icon-check"></i> Complete to-do
                            </a>
                            <a class="dropdown__button js-button-expand" data-expand="due">
                                <i class="dropdown__icon"></i> Set next due... <i class="dropdown__nest-indicator icon-down-dir"></i>
                            </a>
                            <div class="js-expand-due" style="display: none;">
                                <div class="dropdown__data"> Today  </div>
                                <a href="javascript:;" class="dropdown__button dropdown__button--nested js-button-due" data-target="none"><i class="dropdown__icon"></i> <em>None</em>
                                </a>
                                <a class="dropdown__button dropdown__button--nested js-button-due" data-target="today" style="display: none">
                                    <i class="dropdown__icon"></i> Today
                                </a>
                                <a class="dropdown__button dropdown__button--nested js-button-due" data-target="tomorrow">
                                    <i class="dropdown__icon"></i> Tomorrow
                                </a>
                                <a class="dropdown__button dropdown__button--nested js-button-custom-date" data-postpone="no">
                                    <i class="dropdown__icon"></i> Custom...
                                </a>
                            </div>
                            <a class="dropdown__button js-button-delete">
                                <i class="dropdown__icon"></i>
                                <span class="text--delete">Delete</span>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="js-list-later todos">
            <div style="display: block;">
                <div class="todos__divider todos__divider--later"> Later this week</div>
                <ul class="js-todo-children">
                    <li class="todo"><a class="todo__box todo__box--check "><i class="js-status-icon"></i></a>
                        <div class="todo__date"><span class="hint--left" data-hint="Repeat: sporadic">
                                                <i class="icon-cw"></i> </span> Sunday <a class="todo__options">
                                <i class="icon-down-dir"></i> </a>
                        </div>
                        <div class="todo__desc">
                            <a href="#nodes/68494" data-hint="Goal: Inbox" class="todo__name hint--right">wdeffdfsdfsdfsdfs ^</a>
                            <div class="js-streak streak" data-heat="0" style="display: none">
                                <i class="icon-cw"></i>
                                <span></span>
                            </div>
                            <span class="todo__icons">   </span>
                        </div>
                        <div class="dropdown dropdown--shortcuts" style="display: none; opacity: 0;">
                            <div class="dropdown__item dropdown__item--heading">Shortcuts</div>
                            <a class="dropdown__button js-button-todo-toggle" data-status="completed" data-value="true">
                                <i class="dropdown__icon icon-check"></i> Complete to-do
                            </a>
                            <a class="dropdown__button js-button-expand" data-expand="due">
                                <i class="dropdown__icon"></i> Set next due... <i class="dropdown__nest-indicator icon-down-dir"></i>
                            </a>
                            <div class="js-expand-due" style="display: none;">
                                <a class="dropdown__button dropdown__button--nested js-button-due" data-target="today">
                                    <i class="dropdown__icon"></i> Today
                                </a>
                                <a class="dropdown__button dropdown__button--nested js-button-due" data-target="tomorrow">
                                    <i class="dropdown__icon"></i> Tomorrow
                                </a>
                                <a class="dropdown__button dropdown__button--nested js-button-custom-date" data-postpone="no">
                                    <i class="dropdown__icon"></i> Custom...
                                </a>
                            </div>
                            <a class="dropdown__button js-button-delete">
                                <i class="dropdown__icon"></i>
                                <span class="text--delete">Delete</span>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="js-list-next todos todos--last">
            <div style="display: block;">
                <div class="todos__divider todos__divider--next"> Next steps <a class="help-prompt js-help-prompt-next"><i class="icon-help-circled"></i></a></div>
                <ul class="js-todo-children">
                    <li class="todo" style="display: list-item;">
                        <a class="todo__box todo__box--check "><i class="js-status-icon"></i></a>
                        <div class="todo__date"><a class="todo__options"> <i class="icon-down-dir"></i> </a></div>
                        <div class="todo__desc">
                            <a href="#" data-hint="Goal: Inbox" class="todo__name hint--right">anasun\</a>
                            <div class="js-streak streak" data-heat="0" style="display: none">
                                <i class="icon-cw"></i><span></span>
                            </div>
                                                <span class="todo__icons">
                                                </span>
                        </div>
                        <div class="dropdown dropdown--shortcuts" style="top: 165px; right: 10px; display: none;">
                            <div class="dropdown__item dropdown__item--heading">Shortcuts</div>
                            <a class="dropdown__button js-button-todo-toggle" data-status="completed" data-value="true">
                                <i class="dropdown__icon icon-check"></i> Complete to-do
                            </a>
                            <a class="dropdown__button js-button-expand" data-expand="due">
                                <i class="dropdown__icon"></i> Set next due... <i class="dropdown__nest-indicator icon-down-dir"></i>
                            </a>
                            <div class="js-expand-due" style="display: none;">
                                <a class="dropdown__button dropdown__button--nested js-button-due" data-target="today">
                                    <i class="dropdown__icon"></i> Today
                                </a>
                                <a class="dropdown__button dropdown__button--nested js-button-due" data-target="tomorrow">
                                    <i class="dropdown__icon"></i> Tomorrow
                                </a>
                                <a class="dropdown__button dropdown__button--nested js-button-custom-date" data-postpone="no">
                                    <i class="dropdown__icon"></i> Custom...
                                </a>
                            </div>
                            <a class="dropdown__button js-button-delete">
                                <i class="dropdown__icon"></i>
                                <span class="text--delete">Delete</span>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <!-- <a class="js-show-next" style="margin: 20px 35px"><i class="icon-down-dir"></i> Show Next Steps</a> -->
        <div class="js-help-popout-next dropdown dropdown--help-snip help-snip"  >
            <h3 class="help-snip__heading"> Next steps </h3>
            <div class="help-snip__content">
                <p>Steps which don't have a due date will be placed
                    under <em>next steps</em>. This list can be referred to when you're looking for
                    ideas of something productive (but not time-critical) to do which will help move
                    your bigger goals forward.</p>
                <p>The higher an item has been prioritised on your goal map, the closer it will
                    appear to the top of this list. You can reprioritise steps in the goal map by
                    dragging them by their circular icons.
                </p>
            </div>
        </div>
    </div>
</div>