<?php
    use yii\helpers\Url;
?>
<div id="js-page-main" class="t-region t-region--active" style="display: block;">
    <div>
        <div class="padded-content">
            <div class="cols cols--2">
                <div class="cols__col cols__col--first"><h2 class="heading--divider">Interactive Tutorials</h2>
                    <p>
                        Note: these tutorials cover a wide range of features, some of which are more suited to desktop/laptop users, rather than those operating on a touchscreen.
                    </p>
                    <ul class="bullet-list">
                        <li>
                            <a href="<?= Url::home().'goalmap'?>" class="js-show-tutorial" data-tutorial="fundamentals" style="font-weight: bold; padding: 5px 0;">1: Fundamentals (Steps &amp; Goals)</a>
                        </li>
                        <li>
                            <a href="<?= Url::home().'goalmap'?>" class="js-show-tutorial" data-tutorial="trackers" style="font-weight: bold; padding: 5px 0;">2: Trackers &amp; Targets</a>
                        </li>
                    </ul>
                </div>
                <div class="cols__col cols__col--last"><h2 class="heading--divider">Resources</h2>
                    <p>
                        <a href="#" target="_blank" style="font-weight: bold">Help Center &#187 </a><br>
                        A collection guides, examples, FAQs, and articles, are all located in our Help Center.
                    </p>
                    <p>
                        <a href="#" target="_blank" style="font-weight: bold">
                            Adding Nach to your home screen
                            <i class="icon-mobile"></i> &#187 </a><br>
                        A short guide on how to add Nach to your phone or tablet's home screen.
                    </p>
                    <p>
                        <a href="#" target="_blank" style="font-weight: bold">Contact &#187 </a><br>
                        If you have any feedback, questions, or comments, please don't hesitate to get in touch through our contact form.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>