<?php
    use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>

<div class="splash-panel">
    <h1 class="splash-panel__heading">Sign in</h1>
    <?php $form = ActiveForm::begin([
        'action' => ['login'],
        'id' => 'login-form',
        'options' => [ ],
    ]); ?>

    <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label('Username or Email',['class'=>'splash-label']) ?>

    <?= $form->field($model, 'password')->passwordInput()->label('Password',['class'=>'splash-label']) ?>

    <?= Html::submitButton('Sign in', ['class' => 'js-submit splash-panel__button', 'name' => 'login-button']) ?>

    <div style="margin-top: 15px;">
        <a href="/password">Forgotten username / password &#187; </a>
    </div>
    <div style="margin-top: 10px;">
        <a href="/register">Create an account &#187; </a>
    </div>
    <?php ActiveForm::end(); ?>
</div>
