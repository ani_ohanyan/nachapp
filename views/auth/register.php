<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Register';
?>

<div class="splash-panel">
    <h1 class="splash-panel__heading">Create an account</h1>
    <p style="line-height: 1.5; margin-top: 20px; margin-bottom: 0">
        <span class="splash-panel__detail"> Creating a <strong>30-day free trial</strong> account.<br> Fill in the form below to get started. </span>
    </p>
    <?php $form = ActiveForm::begin([
        'action' => ['register'],
        'options' => [ ],
    ]); ?>

        <div>
            <label class="splash-label required" >Username</label>
            <div class="splash-hint hint--bottom hint--always">
                <?= $form->field($model, 'username')->textInput(['name'=>'nach_user_registration[username]', 'id'=>'nach_user_registration_username', 'class'=>'splash-input', 'placeholder' =>'Letters/numbers only - no spaces', 'for' => 'nach_user_registration_username', 'maxlength'=> '20', 'pattern' => "[A-Za-z0-9]+"])->label(false); ?>
            </div>
            <span style="color: #800; font-size: 1.4rem;"></span>
        </div>
        <div>
            <label class="splash-label required" for="nach_user_registration_email">Email</label>
            <div class="splash-hint hint--bottom hint--always">
                <?= $form->field($model, 'email')->input('email', ['name'=>'nach_user_registration[email]','id'=>'nach_user_registration_email', 'class'=>'splash-input',  'maxlength'=> '60'])->label(false); ?>
            </div>
            <span style="color: #800; font-size: 1.4rem;"></span>
        </div>
        <div>
            <label class="splash-label required" for="nach_user_registration_plainPassword_first">Password</label>
            <div class="splash-hint hint--bottom hint--always">
                <?= $form->field($model, 'password')->passwordInput(['name'=>'nach_user_registration[plainPassword][first]', 'id'=>'nach_user_registration_plainPassword_first', 'class'=>'splash-input',  'placeholder'=> 'At least 6 characters'])->label(false); ?>
            </div>
            <span style="color: #800; font-size: 1.4rem;"></span>
        </div>
        <div><label class="splash-label required" for="nach_user_registration_plainPassword_second">Confirm  password</label>
            <div class="splash-hint hint--bottom hint--always">
                <?= $form->field($model, 'password')->passwordInput(['name'=>'nach_user_registration[plainPassword][second]', 'id'=>'nach_user_registration_plainPassword_second', 'class'=>'splash-input'])->label(false); ?>
            </div>
            <span style="color: #800; font-size: 1.4rem;"></span>
        </div>
        <p style="font-size: 14px; font-size: 1.4rem; margin: 15px 0 0">By clicking Register, I agree to the
            <a  href="/terms" target="_blank">Terms of Service</a> and
            <a href="/privacy" target="_blank">Privacy Policy</a>
        </p>
        <?= Html::hiddenInput('nach_user_registration[timezone]', 'Asia/Dubai', ['id' => 'nach_user_registration_timezone']);?>
        <div>
            <?= Html::submitButton('Register', ['class' => 'splash-panel__button', 'name' => 'nach_user_registration[save]', 'id'=>'nach_user_registration_save']) ?>
        </div>
         <?php ActiveForm::end(); ?>
</div>