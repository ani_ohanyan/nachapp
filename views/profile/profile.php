<div id="js-page-main" class="t-region t-region--active" style="display: block;">
    <div>
        <div class="padded-content">
            <div class="cols cols--2">
                <div class="cols__col cols__col--first" style="overflow: hidden;">
                    <div class="profile-avatar">
                        <img src="//images.nachapp.com/bundles/nachgoal/images/default_avatar.gif" class="js-avatar avatar avatar--large">
                        <div class="profile-avatar__overlay">
                            <a class="js-avatar-change dz-clickable">chang </a>
                            <input type="file" id="avatar_input" style="opacity: 0; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;">

                            <div class="js-noop" style="display: none"></div>
                        </div>
                    </div>
                    <div style="float: left; margin: 8px 20px;"><h1 class="heading--main" style="margin-bottom: 0;">
                            Lavrovsergey</h1>

                        <div style="font-size: 1.5rem; line-height: 1.5"><a href="/logout">Sign out</a></div>
                    </div>
                </div>
                <div class="cols__col cols__col--last"><h2 class="heading--small-caps" style="margin: 20px 0 10px">
                        Network status</h2>
                    <ul>
                        <li style="margin-bottom: 10px">
                            <div class="js-socket socket socket--on" style="margin: 0 5px"></div>
                            <span class="js-socket-message js-socket-message--con"
                                  style="display: inline;">Connected</span> <span
                                class="js-socket-message js-socket-message--dis"
                                style="display: none;">No connection (<a class="js-socket-attempt">try now</a>)</span>
                            <span class="js-socket-message js-socket-message--attempt" style="display: none;">Attempting to connect</span>
                            (<a href="/help/guides/network-status" target="_blank">learn more</a>)
                        </li>
                        <li>
                            <a onclick="window.location.reload(true)" class="hint--bottom" data-hint="Reload the app">
                                <i class="icon-arrows-cw"></i> Reload the app
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <h2 class="heading--divider">Settings</h2>
            <ul class="stats-list">
                <li><span class="stats-list__label">Email:</span> s.lavrov007@gmail.com</li>
                <li>
                    <div class="js-stat-mobile" style="display: inline-block">
                        <div class="editable-stat"><span class="stats-list__label">Mobile:</span>
                            <span class="js-area-read"> <?=$profile['phone_number']?$profile['phone_number']:'none';?>
                                <a class="editable-stat__button editable-stat__edit-prompt js-stat-edit hint--right" data-hint="Edit">
                                    <i class="icon-pencil"></i>
                                </a>
                            </span>
                            <span class="js-area-edit" style="display: none">
                                <span class="js-error hint--bottom hint--always hint--error">
                                    <input name="behat-editable-stat-input" type="text" class="editable-stat__input js-stat-input behat-editable-stat-input editable-stat__input--valid"  value="">
                                </span>
                                <a class="editable-stat__button js-stat-save hint--bottom" data-hint="Save">
                                    <i class="icon-check"></i></a>
                                <a class="editable-stat__button js-stat-discard hint--bottom" data-hint="Cancel">
                                    <i class="icon-cancel"></i>
                                </a>
                            </span>
                        </div>
                    </div>
                    <a class="help-prompt js-help-prompt-mobile"><i class="icon-help-circled"></i></a>
                    <div class="js-help-popout-mobile dropdown dropdown--help-snip help-snip" style="display: none;">
                        <h3 class="help-snip__heading">
                            <i class="icon-mobile"></i> Mobile
                        </h3>
                        <div class="help-snip__content">
                            <p>Providing a mobile number will allow you to enable SMS reminders for steps with a due date and time. (We will never use the number to contact
                                you for any other reason). Make sure you include the country code, for example:</p>
                            <ul class="bullet-list">
                                <li>
                                    <img src="//images.nachapp.com/bundles/nachgoal/images/misc/flags/gb.png?v148" style="margin-right: 6px"> +44 7900 123 456
                                </li>
                                <li>
                                    <img src="//images.nachapp.com/bundles/nachgoal/images/misc/flags/us.png?v148" style="margin-right: 6px"> +1 310 100 2000
                                </li>
                            </ul>
                            <p>SMS reminders are currently supported for mobile numbers in the following countries:</p>
                            <div class="cols cols--2">
                                <div class="cols__col cols__col--first">
                                    <ul class="bullet-list">
                                        <li>Australia (+61)</li>
                                        <li>Belgium (+32)</li>
                                        <li>Canada (+1)</li>
                                        <li>Denmark (+45)</li>
                                        <li>France (+33)</li>
                                        <li>Germany (+49)</li>
                                        <li>India (+91)</li>
                                        <li>Mexico (+52)</li>
                                        <li>Netherlands (+31)</li>
                                        <li>New Zealand (+64)</li>
                                    </ul>
                                </div>
                                <div class="cols__col cols__col--last">
                                    <ul class="bullet-list">
                                        <li>Norway (+47)</li>
                                        <li>Poland (+48)</li>
                                        <li>Saudi Arabia (+966)</li>
                                        <li>Singapore (+65)</li>
                                        <li>Spain (+34)</li>
                                        <li>South Korea (+82)</li>
                                        <li>Sweden (+46)</li>
                                        <li>Switzerland (+41)</li>
                                        <li>United Kingdom (+44)</li>
                                        <li>United States (+1)</li>
                                    </ul>
                                </div>
                            </div>
                            <p>If you'd like us to add support for your country, please <a href="/contact" target="_blank">get in
                                    touch</a>.</p>
                        </div>
                    </div>
                </li>
                <li><span class="stats-list__label">Timezone:</span> Asia / Dubai</li>
                <li><span class="stats-list__label">API Key:</span>
                    <a class="js-show-api-key">show</a>
                    <span class="js-api-key" style="display: none">19VmNJA4vyH4fBMIQyHPOk1gkrwO8Hca
                    </span>
                </li>
            </ul>
            <h2 class="heading--divider">Billing</h2>
            <div class="cols cols--2">
                <div class="cols__col cols__col--first">
                    <ul class="stats-list">
                        <li>
                            <span class="stats-list__label">Plan:</span>
                            Standard
                            (<a href="/billing" target="_blank">Manage</a>)
                        </li>
                        <li>
                            <span class="stats-list__label">Status:</span>
                            <i class="icon-check"></i>
                        </li>
                        <li>
                            <span class="stats-list__label">Trial period ends:</span>
                            03 Jun 2016
                        </li>
                    </ul>
                </div>
                <div class="cols__col cols__col--last">
                    <ul class="stats-list">
                        <li><span class="stats-list__label">SMS reminders (this month):</span> 0 / 100</li>
                        <li><span class="stats-list__label">Attachments:</span> 0 MB / 1000 MB</li>
                    </ul>
                </div>
            </div>
            <h2 class="heading--divider">Choose a background</h2>
            <p class="notice-bg">Note: Backgrounds are only visible when using the app on a large screen.</p>
            <div class="profile-backgrounds js-backgrounds">
                <img data-slug="warm" class="profile-backgrounds__item " src="//images.nachapp.com/bundles/nachgoal/images/background_choices/warm.jpg">
                <img data-slug="cool" class="profile-backgrounds__item " src="//images.nachapp.com/bundles/nachgoal/images/background_choices/cool.jpg">
                <img data-slug="swirls" class="profile-backgrounds__item " src="//images.nachapp.com/bundles/nachgoal/images/background_choices/swirls.jpg">
                <img data-slug="bokeh1" class="profile-backgrounds__item " src="//images.nachapp.com/bundles/nachgoal/images/background_choices/bokeh1.jpg">
                <img data-slug="bokeh2" class="profile-backgrounds__item " src="//images.nachapp.com/bundles/nachgoal/images/background_choices/bokeh2.jpg">
                <img data-slug="deadsea" class="profile-backgrounds__item " src="//images.nachapp.com/bundles/nachgoal/images/background_choices/deadsea.jpg">
                <img data-slug="haze" class="profile-backgrounds__item " src="//images.nachapp.com/bundles/nachgoal/images/background_choices/haze.jpg">
                <img data-slug="chiemsee" class="profile-backgrounds__item " src="//images.nachapp.com/bundles/nachgoal/images/background_choices/chiemsee.jpg">
                <img data-slug="grass" class="profile-backgrounds__item " src="//images.nachapp.com/bundles/nachgoal/images/background_choices/grass.jpg">
                <img data-slug="alpine" class="profile-backgrounds__item profile-backgrounds__item--selected" src="//images.nachapp.com/bundles/nachgoal/images/background_choices/alpine.jpg">
            </div>
            <div class="attribution" style="margin-top: 5px"></div>
            <h2 class="heading--divider">Email preferences</h2>
            <ul class="email-prefs">
                <li>
                    <input class="js-email-pref" data-pref="WeeklySummary" type="checkbox" checked="">
                    <label> Send me a weekly summary every Monday morning
                        <div>A recap of how much you got done last week, and a copy of this week's to-do list</div>
                    </label>
                </li>
                <li>
                    <input class="js-email-pref" data-pref="DailySummary" type="checkbox" checked="">
                    <label> Send me a
                        daily to-do list summary
                        <div>Sends in the morning, only when you have something current on your to-do list</div>
                    </label>
                </li>
                <li>
                    <input class="js-email-pref" data-pref="Newsletter" type="checkbox" checked="">
                    <label> Send me an
                        occasional newsletter
                        <div>Sent every few weeks, and covers new features and announcements for Nach (don't worry - it
                            doesn't contain ads)
                        </div>
                    </label>
                </li>
            </ul>
            <h2 class="heading--divider">Labs</h2>
            <div style="max-width: 640px">
                <p>Add extra functionality to Nach by enabling some of our experimental features below, and
                    <a href="/contact" target="_blank">give your feedback</a>.
                    You may need to reload the page before a feature takes full effect.
                </p>
                <ul style="margin: 15px 0 25px">
                    <li class="lab">
                        <div class="lab__left"><img class="lab__thumb" src="//images.nachapp.com/bundles/nachgoal/images/misc/features/failed.png?v148">

                            <div class="lab__toggle">
                                <input type="checkbox" class="js-lab-toggle" data-feature="failed"> Enabled
                            </div>
                        </div>
                        <div class="lab__right"><strong class="lab__title">Failed Goals and Steps</strong>

                            <div class="lab__desc">
                                <p>
                                    Instead of only being able to <em>complete</em> goals and steps, add the ability to mark them as <strong>failed</strong> from their shortcut menus. This will clear them out of your way, as well as showing up in reports.
                                </p>
                                <a class="js-lab-more lab__more">
                                    <i class="icon-down-dir"></i> Show more
                                </a>
                                <p style="display: none">The "failed" status can be useful for things which you missed
                                    the opportunity for, or put some effort into but weren't able to accomplish. E.g.
                                    the step "Apply for tickets" could be failed if it was left too late and is now
                                    impossible.</p>

                                <p style="display: none">
                                    Failed steps and goals will show up as failed in your reports too, so it can be a useful way to determine how well you're sticking to the goals and steps you laid out.
                                </p>
                            </div>
                        </div>
                    </li>
                    <li class="lab">
                        <div class="lab__left"><img class="lab__thumb" src="//images.nachapp.com/bundles/nachgoal/images/misc/features/keyboard.png?v148">
                            <div class="lab__toggle">
                                <input type="checkbox" class="js-lab-toggle" data-feature="keyboard"> Enabled
                            </div>
                        </div>
                        <div class="lab__right">
                            <strong class="lab__title">Keyboard Shortcuts</strong>
                            <div class="lab__desc">
                                <p>
                                    For desktop users, enables some simple navigational keyboard shortcuts. Refresh, then press <strong>?</strong> to see available shortcuts. Watch this space - more advanced keyboard shortcuts coming soon.
                                </p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <h2 class="heading--divider">Delete account</h2>
            <div style="max-width: 640px">
                <p>
                    If you'd like to delete your Nach account, use the button below. You will no longer receive any emails or other contact from Nach. Please note that this action is permanent, and you will lose all goals, trackers, notes, and other information you have saved with the app.
                </p>
                <a class="button" href="/account/close">Delete account</a>
            </div>
        </div>
    </div>
</div>