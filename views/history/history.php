<div id="js-page-main" class="t-region t-region--active" style="display: block;">
    <div>
        <div class="padded-content">
            <div class="history-top">
                <div class="history-selector"><select class="form__input js-month-select">
                        <option value="" selected="">Last 30 days</option>
                        <option value="2016-05"> May 2016</option>
                    </select></div>
                <label class="interactive-toggle history-show-neg"> <input type="checkbox" class="js-show-neg"> Show
                    negative actions </label></div>
            <div class="js-graph">
            </div>
            <div class="journal-compose js-journal-compose"><span>Add a note about today:</span>
                <textarea class="form__input js-journal-input" style="max-width: 100%; height: 60px"></textarea>
                <div class="js-journal-buttons" style="margin-top: 8px; display: none;">
                    <a class="button button--cta js-journal-save">Save</a> <a class="button js-journal-discard">Cancel</a>
                </div>
            </div>
            <div class="js-date js-date-2016-05-16"><h3 class="history-bullets__heading">Today</h3>
                <ul class="history-bullets__list">
                    <li class="history-bullets__bullet"><i class="emblem emblem--small bg--tracker icon-comment"></i>
                        <undefined class="history-note js-note-id-10377">
                            <div class="js-area-content">
                                <div class="note-content js-note-content">xasxsax <a class="js-edit-note hint--bottom" data-hint="Edit note"><i
                                            class="icon-pencil text--link"></i></a></div>
                                <div class="js-see-more-cont" style="display: none">
                                    <a class="js-see-more">Show more <span></span></a>
                                    <a class="js-edit-note hint--bottom" data-hint="Edit note"><i class="icon-pencil text--link"></i></a>
                                </div>
                            </div>
                            <div class="js-area-edit" style="display: none; background: #fafafa; padding: 8px">
                                <div style="margin: 6px 0 10px">
                                    <span class="hidden-mobile" style="margin-right: 5px;">Created</span>
                                    <div class="js-date-picker" style="display: inline-block;">
                                        <div class="date-time-picker">
                                            <div class="hint--bottom hint--always hint--error"
                                                 style="display: inline-block; vertical-align: top"><input
                                                    class="form__input measure-row__input-rec-date js-input-date"
                                                    type="text" value="16 May 2016" placeholder="Date"
                                                    style="width: 100px; border-top-right-radius: 0; border-bottom-right-radius: 0; border-right-width: 0;">
                                            </div>
                                            <a class="js-pickadate date-time-picker__cal-button picker__input" data-value="16 May 2016" id="P654122203" type="text" aria-haspopup="true"  aria-expanded="false" aria-readonly="false" aria-owns="P654122203_root"><i class="icon-calendar"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <textarea class="js-input form__input history-note__textarea"
                                          style="margin: 6px 0 2px;">xasxsax</textarea>

                                <div class="history-note__buttons" style="margin-bottom: 3px;">
                                    <a class="button button--cta js-save">Save</a>
                                    <a class="button js-cancel">Cancel</a>
                                    <a class="button button--delete button--icon-solo js-remove">
                                        <i class="button__icon button__icon--solo icon-trash"></i> Delete note
                                    </a>
                                </div>
                            </div>
                        </undefined>
                    </li>
                    <li class="history-bullets__bullet"><i class="emblem emblem--small bg--tracker icon-comment"></i>
                        <undefined class="history-note js-note-id-10378">
                            <div class="js-area-content">
                                <div class="note-content js-note-content">ergtert <a class="js-edit-note hint--bottom"
                                                                                     data-hint="Edit note"><i
                                            class="icon-pencil text--link"></i></a></div>
                                <div class="js-see-more-cont" style="display: none"><a class="js-see-more">Show more
                                        <span></span></a> <a class="js-edit-note hint--bottom" data-hint="Edit note"><i
                                            class="icon-pencil text--link"></i></a></div>
                            </div>
                            <div class="js-area-edit" style="display: none; background: #fafafa; padding: 8px">
                                <div style="margin: 6px 0 10px"><span class="hidden-mobile" style="margin-right: 5px;">Created</span>

                                    <div class="js-date-picker" style="display: inline-block;">
                                        <div class="date-time-picker">
                                            <div class="hint--bottom hint--always hint--error"
                                                 style="display: inline-block; vertical-align: top"><input
                                                    class="form__input measure-row__input-rec-date js-input-date"
                                                    type="text" value="16 May 2016" placeholder="Date"
                                                    style="width: 100px; border-top-right-radius: 0; border-bottom-right-radius: 0; border-right-width: 0;">
                                            </div>
                                            <a class="js-pickadate date-time-picker__cal-button picker__input"
                                               data-value="16 May 2016" id="P1696530488" type="text"
                                               aria-haspopup="true" aria-expanded="false" aria-readonly="false"
                                               aria-owns="P1696530488_root"><i class="icon-calendar"></i></a>

                                        </div>
                                    </div>
                                </div>
                                <textarea class="js-input form__input history-note__textarea"
                                          style="margin: 6px 0 2px;">ergtert</textarea>

                                <div class="history-note__buttons" style="margin-bottom: 3px;"><a
                                        class="button button--cta js-save">Save</a> <a
                                        class="button js-cancel">Cancel</a> <a
                                        class="button button--delete button--icon-solo js-remove"><i
                                            class="button__icon button__icon--solo icon-trash"></i> Delete note</a>
                                </div>
                            </div>
                        </undefined>
                    </li>
                </ul>
            </div>

            <div class="js-date js-date-2016-05-11" style="display: none">
                <h3 class="history-bullets__heading">Wed 11 May</h3>
                <ul class="history-bullets__list">
                    <li class="history-bullets__bullet"><i class="emblem emblem--small bg--missed-step icon-flag"></i>
                        Missed 3 steps<a class="js-steps-expand" data-type="step-missed"><i class="icon-plus"></i></a>
                    </li>
                    <li class="history-bullets__bullet js-steps-granular" data-type="step-missed" style="margin-bottom: 5px; display: none;"><i class="emblem emblem--small icon-flag"></i>
                        <a href="#nodes/68494">wdeffdfsdfsdfsdfs ^</a></li>
                    <li class="history-bullets__bullet js-steps-granular" data-type="step-missed" style="margin-bottom: 5px; display: none;">
                        <i class="emblem emblem--small icon-flag"></i>
                        <a href="#nodes/69260">^</a>
                    </li>
                    <li class="history-bullets__bullet js-steps-granular" data-type="step-missed" style="margin-bottom: 5px; display: none;">
                        <i class="emblem emblem--small icon-flag"></i>
                        <a href="#nodes/69285">^ ggg</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
