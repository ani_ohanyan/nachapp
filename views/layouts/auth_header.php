<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="overall-bg--warm wf-proximanova-i3-active wf-proximanova-n3-active wf-proximanova-n6-active wf-active">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="no-touch">
<?php $this->beginBody(); ?>
<div class="overflow-container" style="width: 100%;overflow-x: hidden;" >
    <div style="position: relative">
        <div class="external-header">
            <div class="hcont">
                <a href="/" class="external-header__logo">
                    <img src="//images.nachapp.com/bundles/nachgoal/images/design/logo-beta.png?v148" alt="Nach">
                </a>
                <ul class="external-header__menu">
                    <li class="external-header__menu-item hidden-mobile "><a href="/blog">Blog</a></li>
                    <li class="external-header__menu-item external-header__menu-item--active"><a href="/pricing">Pricing</a></li>
                    <li class="external-header__menu-item "><a href="/help">Help</a></li>
                    <li class="external-header__menu-item "><a href="/contact">Contact</a></li>
                    <li class="external-header__menu-item external-header__menu-item--hilit"><a href="/login">Sign in</a></li>
                    <li class="external-header__menu-item external-header__menu-item--hilit"><a href="/register">Register</a> </li>
                </ul>
            </div>
        </div>

        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>

    </div>
</div>

<!--  footer  -->
<div class="external-footer">
    <div class="hcont">
        <div class="external-footer__cols">
            <div class="external-footer__col"><span class="external-footer__heading"> About </span>
                <ul class="external-footer__links">
                    <li><a href="/">Product Overview</a></li>
                    <li><a href="/about">About Us</a></li>
                    <li><a href="/pricing">Plans &amp; Pricing</a></li>
                    <li><a href="/roadmap">Roadmap</a></li>
                </ul>
            </div>
            <div class="external-footer__col"><span class="external-footer__heading"> Resources </span>
                <ul class="external-footer__links">
                    <li><a href="/help">Help Center</a></li>
                    <li><a href="/help/quickstart">Quick Start Guide</a></li>
                    <li><a href="/help/faq">FAQ</a></li>
                    <li><a href="/help/guides">Feature Guides</a></li>
                    <li><a href="/help/examples">Usage Examples</a></li>
                </ul>
            </div>
            <div class="external-footer__col"><span class="external-footer__heading"> Follow </span>
                <ul class="external-footer__links">
                    <li><a href="/blog"><i class="icon-rss"></i> Blog</a></li>
                    <li><a href="https://www.facebook.com/nachapp" target="_blank"><i class="icon-facebook-squared"></i> Facebook</a></li>
                    <li><a href="https://twitter.com/nachapp" target="_blank"><i class="icon-twitter"></i> Twitter</a></li>
                </ul>
            </div>
            <ul class="external-footer__col external-footer__col--right">
                <li><a class="external-footer__button" href="/register">Create an account</a></li>
                <li><a class="external-footer__button" href="/login">Sign in</a></li>
            </ul>
        </div>
        <div class="external-footer__base"> Copyright &#169; 2015 <span class="external-footer__links" style="float: right">
                <a href="/terms">Terms of service</a> &#8226; <a href="/privacy">Privacy policy</a> </span>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
