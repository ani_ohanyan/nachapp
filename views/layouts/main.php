<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="overall-bg--bokeh1 wf-proximanova-n3-active wf-proximanova-n6-active wf-proximanova-i3-active wf-active">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="no-touch">
<?php $this->beginBody(); ?>
<div class="overflow-container">
    <div>
        <div class="js-live-bg-exclusion app-overall">
            <ul id="js-sidebar" class="app-sidebar">
                <li class="app-sidebar__item js-tab-profile app-sidebar__item--right app-sidebar__item--user <?=trim(Yii::$app->controller->id) == 'profile'?'app-sidebar__item--selected':''?>">
                    <a href="<?= Url::home().'profile'?>" class="app-sidebar__item-inner " style="position: relative;">
                        <img class="js-avatar avatar avatar--nav"/>
                        <span class="js-username"></span>
                        <div class="js-socket socket app-sidebar__socket"></div>
                    </a>
                </li>
                <li class="app-sidebar__item js-tab-search <?=trim(Yii::$app->controller->id) == 'search'?'app-sidebar__item--selected':''?>">
                    <a href="<?= Url::home().'search'?>" class="app-sidebar__item-inner">
                        <i class="app-sidebar__item-icon icon-search"></i>
                        <span>Search</span>
                    </a>
                </li>
                <li class="app-sidebar__item js-tab-help hint--right hint--always <?=trim(Yii::$app->controller->id) == 'help'?'app-sidebar__item--selected':''?>">
                    <a href="<?= Url::home().'help'?>" class="app-sidebar__item-inner">
                        <i class="app-sidebar__item-icon icon-help"></i>
                        <span>Help</span>
                    </a>
                </li>
                <li class="app-sidebar__heading"> Create</li>
                <li class="app-sidebar__item js-tab-goal <?=trim(Yii::$app->controller->id) == 'goalmap'?'app-sidebar__item--selected':''?>">
                    <a href="<?= Url::home().'goalmap'?>" class="app-sidebar__item-inner">
                        <i class="app-sidebar__item-icon icon-flow-cascade"></i>
                        <span>Goal Map</span>
                    </a>
                </li>
                <li class="app-sidebar__heading"> Daily updates</li>
                <li class="app-sidebar__item js-tab-step <?=trim(Yii::$app->controller->id) == 'todo'?'app-sidebar__item--selected':''?>">
                    <a href="<?= Url::home().'todo'?>" class="app-sidebar__item-inner">
                        <i class="app-sidebar__item-icon icon-checkbox"></i>
                        <span>To-Do List</span>
                    </a>
                </li>
                <li class="app-sidebar__item js-tab-calendar <?=trim(Yii::$app->controller->id) == 'calendar'?'app-sidebar__item--selected':''?>">
                    <a href="<?= Url::home().'calendar'?>" class="app-sidebar__item-inner">
                        <i class="app-sidebar__item-icon icon-calendar"></i>
                        <span>Calendar</span>
                    </a>
                </li>
                <li class="app-sidebar__item js-tab-tracker <?=trim(Yii::$app->controller->id) == 'tracker'?'app-sidebar__item--selected':''?>">
                    <a href="<?= Url::home().'tracker'?>" class="app-sidebar__item-inner">
                        <i class="app-sidebar__item-icon icon-chart-line"></i>
                        <span>Trackers</span>
                    </a>
                </li>
                <li class="app-sidebar__heading"> Reports</li>
                <li class="app-sidebar__item js-tab-history <?=trim(Yii::$app->controller->id) == 'history'?'app-sidebar__item--selected':''?>">
                    <a href="<?= Url::home().'history'?>" class="app-sidebar__item-inner">
                        <i class="app-sidebar__item-icon icon-clock"></i>
                        <span>History</span>
                    </a>
                </li>
                <li class="app-sidebar__item js-tab-graphs <?=trim(Yii::$app->controller->id) == 'graphs'?'app-sidebar__item--selected':''?>">
                    <a href="<?= Url::home().'graphs'?>" class="app-sidebar__item-inner">
                        <i class="app-sidebar__item-icon icon-chart-bar"></i>
                        <span>Graphs</span>
                    </a>
                </li>
                <li class="app-sidebar__heading"> Settings</li>
                <li class="app-sidebar__item js-tab-categories <?=trim(Yii::$app->controller->id) == 'categories'?'app-sidebar__item--selected':''?>">
                    <a href="<?= Url::home().'categories'?>" class="app-sidebar__item-inner">
                        <i class="app-sidebar__item-icon icon-folder"></i>
                        <span>Categories</span>
                    </a>
                </li>
                <li class="app-sidebar__footer">
                    <a href="#" target="_blank">Home</a> |
                    <a href="#" target="_blank">Help</a> |
                    <a href="#" target="_blank">Contact</a><br/>
                    <a href="#" target="_blank">Blog</a> |
                    <a href="#" target="_blank">Roadmap</a>
                </li>
            </ul>
            <div class="app-content js-app-content">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= $content ?>
            </div>
        </div>
    </div>
</div>

<!--<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?/*= date('Y') */?></p>
        <p class="pull-right"><?/*= Yii::powered() */?></p>
    </div>
</footer>-->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
