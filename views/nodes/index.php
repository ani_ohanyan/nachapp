<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
//$this->registerCssFile('@web/css/dropzone/dropzone.css');
?>

<div id="js-app" class="nodes_page">
    <div id="js-page-main" class="t-region t-region--active" style="display: block;">
        <div>
            <div class="node-detail__main js-edit">
                <div class="padded-content" style="min-height: 13px">
                    <div class="node-detail-buttons">
                        <a href="javascript:;" class="button button--icon-left js-start-edit">
                            <i class="button__icon icon-pencil"></i>
                            <span class="button__label">Edit</span>
                        </a>
                        <a href="javascript:;" class="button button--icon-solo js-more-buttons">
                            <i class="button__icon icon-down-dir"></i>
                        </a>

                        <div class="js-shortcut-menu">
                            <div class="dropdown dropdown--shortcuts dropdown--visible" style="top: 82px; right: 37px; display: none;">
                                <a href="javascript:;" class="dropdown__button js-button-expand"  data-expand="due">
                                    <i class="dropdown__icon"></i> Set next due...
                                    <i class="dropdown__nest-indicator icon-down-dir"></i>
                                </a>
                                <div class="js-expand-due" style="display: none">
                                    <a href="javascript:;" class="dropdown__button dropdown__button--nested js-button-due" data-target="today">
                                        <i class="dropdown__icon"></i> Today
                                    </a>
                                    <a href="javascript:;" class="dropdown__button dropdown__button--nested js-button-due" data-target="tomorrow">
                                        <i class="dropdown__icon"></i> Tomorrow
                                    </a>
                                    <a href="javascript:;" class="dropdown__button dropdown__button--nested js-button-custom-date" data-postpone="no">
                                        <i class="dropdown__icon"></i> Custom...
                                    </a>
                                </div>
                                <a href="javascript:;" class="dropdown__button js-button-archived-toggle" data-action="archive">
                                    <i class="dropdown__icon icon-archive"></i> Archive
                                </a>
                                <a  href="javascript:;" class="dropdown__button js-button-delete">
                                    <i class="dropdown__icon"></i>
                                    <span class="text--delete">Delete</span>
                                </a>
                            </div>

                        </div>
                    </div>
                    <div style="margin-bottom: 10px;">
                        <i class="emblem emblem--small bg--inbox icon-inbox"></i>
                        <a class="subtle-link" href="<?=Url::to(['/nodes', 'id'=>$data['parent_id']])?>"><?=$data['parent_id'] == 0?'Inbox':$parent['name']?></a> &#187;
                    </div>
                </div>
                <div class="padded-content">
                    <h1 class="heading--main" style="margin-top: 0;">
                        <div class="hint--bottom" style="display: inline-block">
                            <i class="emblem emblem--big bg--step icon-flag"></i>
                        </div>
                        <?=$data['name'];?>
                        <div class="js-streak streak hint--right" ></div>
                    </h1>
                </div>
                <div class="js-completed-view"></div>
                <div class="padded-content">
                    <ul class="stats-list" style="margin-top: 25px;">
                        <li><i class="stats-list__icon icon-calendar"></i>
                            <span class="stats-list__label">Due:</span>
                            Thu 26 May <span style="color: #ccc">-</span>
                            <span class="text--light">in 8 days</span>
                        </li>
                        <li><i class="stats-list__icon icon-cw"></i>
                            <span class="stats-list__label">Repeat:</span>
                            <span>  sporadic  </span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="js-todo-cont" style="border-style: solid; border-color: rgb(238, 238, 238); border-width: 0px 0px 1px; margin: 0px 0px 30px; display: block;">
                <div class="todos__divider" style="margin-top: 40px">To-do</div>
                <ul class="js-todo">
                    <li class="todo">
                        <a class="todo__box todo__box--check "><i class="js-status-icon"></i></a>
                        <div class="todo__date">
                            <span> <i class="icon-cw"></i> </span>
                            <a class="todo__options">
                                <i  class="icon-down-dir"></i>
                            </a>
                        </div>
                        <div class="todo__desc">
                            <span class="todo__name"><?=$data['name'];?></span>
                        </div>
                        <div class="dropdown dropdown--shortcuts dropdown--visible" style="display: none;">
                            <div class="dropdown__item dropdown__item--heading">Shortcuts</div>
                            <a href="javascript:;" class="dropdown__button js-button-todo-toggle" data-status="completed" data-value="true">
                                <i class="dropdown__icon icon-check"></i>
                                Complete to-do </a> <a href="javascript:;" class="dropdown__button js-button-expand"  data-expand="due">
                                <i class="dropdown__icon"></i> Set next due... <i class="dropdown__nest-indicator icon-down-dir"></i>
                            </a>
                            <div class="js-expand-due" style="display: none">
                                <div class="dropdown__data"> Thu 26 May</div>
                                <a href="javascript:;" class="dropdown__button dropdown__button--nested js-button-due" data-target="none"> <i class="dropdown__icon"></i> <em>None</em> </a>
                                <a href="javascript:;"  class="dropdown__button dropdown__button--nested js-button-due" data-target="today">
                                    <i class="dropdown__icon"></i> Today
                                </a>
                                <a  href="javascript:;" class="dropdown__button dropdown__button--nested js-button-due" data-target="tomorrow">
                                    <i class="dropdown__icon"></i> Tomorrow
                                </a>
                                <a href="javascript:;" class="dropdown__button dropdown__button--nested js-button-custom-date" data-postpone="no"> <i class="dropdown__icon"></i> Custom... </a>
                            </div>
                            <a href="javascript:;" class="dropdown__button js-button-delete">
                                <i class="dropdown__icon"></i> <span class="text--delete">Delete</span>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
            <h2 class="heading-toggle "><i class="icon-chart-bar"></i> Step history chart </h2>
            <div class="padded-content" style="">
                <div style="padding: 20px 0 0px">
                    <a class="help-prompt js-help-prompt-step-history">
                        <i class="icon-help-circled"></i>
                    </a>
                    <div class="js-total-completions-container" style="float: right;">
                        <span class="stats-list__label">Total completions:</span>
                        <span class="js-total-completions" style="font-weight: 600">1</span>
                    </div>
                    <div class="js-help-popout-step-history dropdown dropdown--help-snip help-snip" style="display: none;">
                        <h3 class="help-snip__heading"> Step history chart </h3>
                        <div class="help-snip__content">
                            <p>The <strong>step history chart</strong> shows a summary of your activity on a step over the past month, with rectangular indicators over certain days:</p>
                            <ul>
                                <li>
                                    <div class="action-result__box" style="margin-right: 5px; background: #f7b746"></div>
                                    You completed the step on that day (by ticking it off your to-do list).
                                </li>
                                <li>
                                    <div class="action-result__box" style="margin-right: 5px; background: #ccc"></div>
                                    You missed doing the step on the date it was due - either by not ticking it off your to-do list, or by postponing it.
                                </li>
                            </ul>
                            <p>The chart also allows you to go back in time and adjust your record of activity for any day in the past month. For example, if you completed this step yesterday, but forgot to  mark it as complete on your to-do list, simply click the rectangular space for yesterday, and click "Completed" in the dropdown list of options that appears.</p>
                            <p>In general, you should still use the to-do list as much as possible from day to day, but this can be a useful backup if you ever want to alter history.</p>
                        </div>
                    </div>
                </div>
                <div class="js-chart" style="padding: 15px 0 0px;">
                </div>
            </div>
            <h2 class="heading-toggle"><i class="icon-clock"></i> Timeline </h2>
            <div class="timeline-container">
                <div class="timeline-spine"></div>
                <div class="js-notes timeline-container__inner">
                    <div class="js-timeline-loading timeline-row" style="display: none">
                        <div class="timeline-row__mark">
                            <div class="timeline-row__bubble timeline-row__bubble--add"></div>
                        </div>
                        <div class="timeline-note">
                            <div class="timeline-note__loading" style="border-width: 0">
                                <i class="icon-spin4 animate-spin loading-orb"></i> Loading <strong class="js-load-value">5</strong> notes...
                            </div>
                        </div>
                    </div>
                    <div class="js-add-buttons timeline-row" style="display: block;">
                        <div class="timeline-row__mark">
                            <div class="timeline-row__bubble timeline-row__bubble--add"><i class="icon-plus"></i></div>
                        </div>
                        <div class="timeline-compose">
                            <a class="js-compose-prompt" data-type="text">
                                <i class="icon-doc-alt"></i> Add a note
                            </a>
                        </div>
                        <div class="timeline-compose">
                            <a class="js-compose-prompt" data-type="image">
                                <i class="icon-picture"></i> Add images
                            </a>
                        </div>
                    </div>
                    <ul>
                        <li class="timeline-row js-note-id-10409">
                            <div class="timeline-row__mark">
                                <div class="timeline-row__bubble">
                                    <i class="icon-picture"></i>
                                </div>
                                <div class="timeline-row__date "> 17 May</div>
                            </div>
                            <div class="timeline-note ">
                                <div class="js-area-loading timeline-note__loading" style="display: none">
                                    <i class="icon-spin4 animate-spin loading-orb"></i> Loading...
                                </div>
                                <div class="js-area-content">
                                    <div class="timeline-note__top"><span class="timeline-tag">Note</span>
                                        <span class="timeline-note__date">  <i class="icon-clock"></i> Posted 17 May 2016   </span>
                                        <a class="js-edit-note timeline-note__button"> <i class="icon-pencil"></i> Edit
                                        </a></div>
                                    <div class="timeline-note__content note-content js-note-content "></div>
                                    <div class="js-see-more-cont timeline-note__see-more" style="display: none">
                                        <a class="js-see-more">Show more <span></span></a>
                                    </div>
                                    <ul class="timeline-note__attachments js-attachments">
                                        <li class="timeline-attachment js-attachment-id-728 timeline-attachment--image">
                                            <a class="timeline-attachment__thumb-link" href="//images.nachapp.com/uploads/attachments/Lavrovsergey/original/728__bae0ff23c0962cbee7b22d2a52686efd.jpg"
                                               target="_blank">
                                                <img src="//images.nachapp.com/uploads/attachments/Lavrovsergey/small/728__0e8c20ab1777704b64a745272202d6f1.jpg">
                                            </a>
                                            <div class="timeline-attachment__details">
                                                <a href="//images.nachapp.com/uploads/attachments/Lavrovsergey/original/728__bae0ff23c0962cbee7b22d2a52686efd.jpg"
                                                    target="_blank"> juc.jpg </a>&nbsp; <span>(239 KB)</span>
                                                <span class="timeline-attachment__buttons">
                                                    <a class="js-delete timeline-attachment__button hint--right" data-hint="Delete">
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                </span>
                                            </div>
                                        </li>
                                        <li class="timeline-attachment js-attachment-id-728 timeline-attachment--image">
                                            <a class="timeline-attachment__thumb-link" href="//images.nachapp.com/uploads/attachments/Lavrovsergey/original/728__bae0ff23c0962cbee7b22d2a52686efd.jpg"
                                               target="_blank">
                                                <img src="//images.nachapp.com/uploads/attachments/Lavrovsergey/small/728__0e8c20ab1777704b64a745272202d6f1.jpg">
                                            </a>
                                            <div class="timeline-attachment__details">
                                                <a href="//images.nachapp.com/uploads/attachments/Lavrovsergey/original/728__bae0ff23c0962cbee7b22d2a52686efd.jpg"
                                                   target="_blank"> juc.jpg </a>&nbsp; <span>(239 KB)</span>
                                                <span class="timeline-attachment__buttons">
                                                    <a class="js-delete timeline-attachment__button hint--right" data-hint="Delete">
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                </span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="js-area-edit timeline-note__edit" style="display: none">
                                    <div class="timeline-note__edit-top">
                                        <div class="timeline-note__edit-top-block">
                                            <span class="hidden-mobile" style="margin-right: 5px;">Created</span>
                                            <div class="js-date-picker"
                                                 style="display: inline-block; margin-right: 15px;">
                                                <div class="date-time-picker">
                                                    <div class="hint--bottom hint--always hint--error"
                                                         style="display: inline-block; vertical-align: top">
                                                        <input class="form__input measure-row__input-rec-date js-input-date" type="text" value="17 May 2016" placeholder="Date"    style="width: 100px; border-top-right-radius: 0; border-bottom-right-radius: 0; border-right-width: 0;">
                                                    </div>
                                                    <a class="js-pickadate date-time-picker__cal-button picker__input" data-value="17 May 2016" id="P1321453536" type="text"  aria-haspopup="true" aria-expanded="false" aria-readonly="false"  aria-owns="P1321453536_root">
                                                        <i class="icon-calendar"></i>
                                                    </a>
                                                    <div class="picker" id="P1321453536_root" aria-hidden="true">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="timeline-note__edit-pin-cont">
                                                <input type="checkbox" id="label-pin"  class="js-edit-pinned">
                                                <label for="label-pin">Pin to top</label>
                                                <a class="help-prompt js-help-prompt-pin">
                                                    <i class="icon-help-circled"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="timeline-note__edit-top-block--right">
                                            <input type="checkbox" id="label-archived" class="js-edit-archived">
                                            <label for="label-archived">Archived</label>
                                        </div>
                                    </div>
                                    <ul class="form timeline-note__edit-main-form">
                                        <li class="form__row form__row--first">
                                            <a href="/help/guides/notes" target="_blank" style="float: right; position: relative; top: -3px">formatting  help</a>
                                            <div class="form__row-heading-text">Content</div>
                                            <textarea class="js-input form__input timeline-note__input" style="margin-top: -1px; "></textarea>
                                        </li>
                                        <div class="js-help-popout-pin dropdown dropdown--help-snip help-snip" style="display: none;">
                                            <h3 class="help-snip__heading"><i class="icon-pin"></i> Pinned notes </h3>
                                            <div class="help-snip__content">
                                                <p>Notes are by default shown on the timeline in order of when they were first recorded, from newest to oldest. This is most relevant in long on-going goals or steps, where  you can record notes relevant to a specific point in time, and look  back on them with the important context of when they were written.</p>
                                                <p>On the other hand, some notes are always relevant (such as a list of links relating to a goal which you are regularly updating). You can <em>pin</em> notes to keep them stuck to the top of the timeline. </p>
                                            </div>
                                        </div>
                                        <li class="form__row" style="">
                                            <div class="form__row-heading-text">Add Attachments</div>
                                            <div class="js-dropzone dropzone dz-clickable">
                                                <div class="dz-default dz-message">
                                                    <span>Drop files here to upload</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="form__row form__row--last form__row--buttons" style="margin-top: 25px"><a class="button button--cta js-save">Save</a>
                                            <a class="button js-cancel">Cancel</a>
                                            <a class="button button--delete button--icon-left js-remove">
                                                <i   class="button__icon icon-trash"></i> Delete note
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
