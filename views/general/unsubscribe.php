<div class="external-block external-block--light" style="padding-bottom: 60px;">
    <div class="hcont"><h1 class="external-title" style="color: inherit"><a href="/billing" style="color: inherit">Billing</a>
        </h1>

        <h1 class="heading--divider">Cancel subscription</h1>

        <p>Are you sure you want to cancel your subscription? This will end any monthly billing to your credit card, but
            also revoke your access to Nach. Your account will be placed in read-only mode until you re-subscribe to a
            plan.</p>
        <form name="form" method="post">
            <div id="form">
                <div>
                    <button type="submit" id="form_save" name="form[save]" class="button button">Cancel subscription </button>
                </div>
                <input type="hidden" id="form__token" name="form[_token]" value="r-OYcV898S-w1GVuGwDJDOo-PJ8uutMussnolVD1Vs8">
            </div>
        </form>
    </div>
</div>