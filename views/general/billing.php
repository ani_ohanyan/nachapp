<div class="external-block external-block--light" style="padding-bottom: 60px;">
    <div class="hcont">
        <h1 class="external-title" style="color: inherit">
            <a href="/billing" style="color: inherit">Billing</a>
        </h1>
        <h1 class="heading--divider">Account Summary</h1>
        <ul class="stats-list">
            <li><span class="stats-list__label">Plan:</span> Standard (<a href="/billing/unsubscribe">Unsubscribe</a>)
            </li>
            <li><span class="stats-list__label">Status:</span> <i class="icon-check"></i></li>
            <li><span class="stats-list__label">Trial period ends:</span> 18 Jun 2016</li>
        </ul>
        <h1 class="heading--divider">Stored Card Details</h1>
        <ul class="stats-list">
            <li><i class="stats-list__icon icon-credit-card"></i> No card on record.</li>
            <li> You are signed up for <strong>monthly</strong> billing of <strong>�3.00</strong>.</li>
            <li style="display: inline-block; margin: 15px 0 20px;">
                <a href="/billing/change-card" class="button">Change billing information</a>
            </li>
        </ul>
        <p>Adding a card will <strong>not</strong> cause you to be billed before your trial ends on 18 Jun 2016.<br>
            It will ensure you have an uninterrupted transition into paid subscription after that date.</p>
    </div>
</div>