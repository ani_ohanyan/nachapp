<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Register';
?>

<div class="external-block external-block--light" style="padding-bottom: 60px;">
    <div class="hcont">
        <div style="max-width: 560px">
                <?php $form = ActiveForm::begin([
                    'action' => ['close'],
                    'options' => [ ],
                ]); ?>

                <h1 class="external-title" style="color: inherit">Delete account</h1>
                <p>Are you SURE you want to delete your account? This action is irreversible, and will permanently delete everything you have stored with Nach.</p>
                <p style="margin: 40px 0 10px;"> Type <strong>DELETE</strong> to confirm the deletion:
                    <?= Html::textInput('form[confirm]', '',[ 'id'=>'form_confirm', 'class'=>'form__input', 'style'=>'width: 120px; margin-left: 10px']); ?>
                </p>
                <div style="color: #f66"></div>
                <p></p>

                <p style="margin-top: 20px">Optionally, it would be helpful if you could tell us briefly why you're  choosing to delete your account:</p>

                <div style="margin: 0 0 30px">
                    <?= Html::textarea('form[reason]', '', ['id' => 'form_reason', 'class'=>"form__input", 'style'=>"height: 60px; max-width: 400px"]);?>
                </div>
                <div>
                    <?= Html::submitButton('Delete my account', ['class' => 'button', 'name' => 'form[save]', 'id'=>'form_save']) ?>
                </div>
                <?= Html::hiddenInput('form[_token]', $user['accessToken'],[ 'id'=>'form__token']); ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>