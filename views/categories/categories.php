<div id="js-page-main" class="t-region t-region--active" style="display: block;">
    <div>
        <div class="padded-content">
            <h2 class="heading--divider">Categories
                <a class="help-prompt js-help-prompt-categories"><i class="icon-help-circled"></i></a>
            </h2>
            <div class="js-help-popout-categories dropdown dropdown--help-snip help-snip" style="display: none;">
                <h3 class="help-snip__heading">
                    <i class="icon-folder"></i> Categories
                </h3>
                <div class="help-snip__content">
                    <p>
                        Categories can be used to separate the steps and goals which relate to different aspects of your life. For example, you may choose to create your categories as: "Fitness", "Work" and "Social".
                    </p>
                    <p>
                        By categorising your goals, you'll be able to quickly filter down to an individual category from the Goal Map, To-Do List or Calendar. It also allows us to generate reports which allow you to look back and see how you're balancing your focus between them.
                    </p>
                    <p>
                        Categories can only be applied to top-level goals. The category given to a top-level goal will cascade down and apply to everything below it.
                    </p>
                </div>
            </div>
            <ul class="js-list categories">
                <?php foreach($category as $data):?>
                    <li class="categories__item">
                        <div class="js-cont-read">
                            <strong style='<?=$data['archive']?"font-weight: normal; font-style: italic;":'font-weight: 600';?>'><?=$data['name'];?></strong>
                            <a class="js-edit"><i class="icon-pencil"></i></a> <br><?=$data['description'];?>
                        </div>
                        <ul class="js-cont-edit form form--boxed" style="display: none">
                            <li class="form__row"><label class="form__label">Category name</label>
                                <input class="form__input js-edit-name" maxlength="40" value="<?=$data['name'];?>">
                            </li>
                            <li class="form__row">
                                <label class="form__label">Description</label>
                                <textarea class="form__input js-edit-desc" maxlength="255" style="height: 80px"><?=$data['description'];?></textarea>
                            </li>
                            <li class="form__row">
                                <input class="js-edit-archived" type="checkbox" <?=$data['archive']?'checked':"";?>> Archive this category
                            </li>
                            <li class="form__row" data_id="<?=$data['id']?>">
                                <a class="js-save button button--cta">Save</a>
                                <a class="js-discard button">Cancel</a>
                                <a class="js-delete button button--delete button--icon-left">
                                    <i class="button__icon icon-trash"></i> Delete category
                                </a>
                            </li>
                        </ul>
                    </li>
                <?php endforeach;?>
            </ul>
            <div style="margin: 30px 0 10px;">Create a new category:</div>
            <input class="form__input js-new" maxlength="40" style="max-width: 200px">
            <a class="button js-save-new">Add</a>
        </div>
    </div>
</div>