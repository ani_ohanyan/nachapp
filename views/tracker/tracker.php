<?php
    use yii\helpers\Url;
?>

<div id="js-page-main" class="t-region t-region--active" style="display: block;">
    <div>
        <ul class="js-tab-strip" style="display: none;">
            <div class="tab-strip">
                <li class="js-tab tab-strip__tab--all tab-strip__tab tab-strip__tab--selected" data-tab="all">
                    <a href="javascript:;" class="tab-strip__anchor">All</a>
                </li>
                <li class="js-tab tab-strip__tab--uncat tab-strip__tab  ui-droppable" data-tab="uncat">
                    <a href="javascript:;" class="tab-strip__anchor">None</a>
                </li>
                <li class="tab-strip__tab tab-strip__tab--edit">
                    <a class="tab-strip__anchor hint--right" href="#categories" data-hint="Manage categories">
                        <i class="icon-pencil"></i>
                    </a>
                </li>
            </div>
        </ul>
        <div class="padded-content">
            <div class="js-no-trackers cols cols--2" style="display: block;">
                <div class="cols__col cols__col--first">
                    <p><strong>Trackers</strong> (<i class="icon-chart-line"></i>)
                        allow you to measure anything you can assign a number to (for example: distance cycled, cigarettes you smoke each day, your bodyweight), and see graphs of your progress over time.
                    </p>
                    <p>Once you're tracking something, you can add
                        <strong>targets</strong> (<i class="icon-target-1"></i>) to make your progress more tangible.
                    </p>

                    <p>
                        <a href="<?= Url::home().'help'?>" style="font-weight: 600">Learn how to use trackers with the interactive tutorial &#187</a>
                    </p>
                    <p>
                        To create your first tracker, visit the page for the goal which you'd like to associate the tracker with, and click "Add a new tracker".
                    </p>
                </div>
                <div class="cols__col cols__col--last">
                    <img src="//images.nachapp.com/bundles/nachgoal/images/misc/tracker.jpg?v148" style="width: 100%">
                </div>
            </div>
            <p class="js-empty-cat" style="display: none">You have no trackers under this category</p>
            <ul class="js-list"></ul>
        </div>
    </div>
</div>