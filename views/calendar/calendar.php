<div id="js-page-main" class="t-region t-region--active" style="display: block;">
    <div>
        <div class="js-tab-strip" style="display: block;">
            <div class="tab-strip">
                <li class="js-tab tab-strip__tab--all tab-strip__tab tab-strip__tab--selected" data-tab="all">
                    <a href="javascript:;" class="tab-strip__anchor">All<span class="tab-strip__count">9</span></a>
                </li>
                <li class="js-tab tab-strip__tab--cat tab-strip__tab  ui-droppable" data-tab="cat-3057">
                    <a href="javascript:;" class="tab-strip__anchor">aaa</a>
                </li>
                <li class="js-tab tab-strip__tab--uncat tab-strip__tab  ui-droppable" data-tab="uncat">
                    <a href="javascript:;" class="tab-strip__anchor">None<span class="tab-strip__count">9</span></a>
                </li>
                <li class="tab-strip__tab tab-strip__tab--edit">
                    <a class="tab-strip__anchor hint--right" href="#categories" data-hint="Manage categories">
                        <i class="icon-pencil"></i>
                    </a>
                </li>
            </div>
        </div>
        <div class="padded-content" style="margin-bottom: 15px;">
            <div style="float: right; text-align: right; margin-bottom: 10px;">
                <a class="button js-show-sub-link" style="margin-right: 0; display: block;">
                    <i class="icon-rss"></i> Show subscription link
                </a>
                <div class="js-sub-link" style="display: none; font-size: 0.8em; max-width: 320px;">
                    <div style="margin-bottom: 8px; font-size: 1.2em"><i class="icon-rss"></i> Subscription link</div>
                    <input value="https://nachapp.com/feed/ical/private/Q7xDR7ImFprV0dDRLlHAFIQ09mPh31sU/nach-all.ics" style="width: 300px;">
                    <div class="text--light" style="margin-top: 5px; line-height: 1.4">Copy the whole of the above link,
                        and subscribe to the URL using a calendar app, such as Google Calendar.<br>
                        <a href="/help/guides/calendar-subscription" target="_blank">More info &#187;</a>
                    </div>
                </div>
            </div>
            <div class="cal-year">
                <div class="cal-year__left">
                    <div class="cal-year__year">2016</div>
                    <div class="cal-year__arrows">
                        <a class="js-change-year" data-dir="prev"><i class="icon-left-dir"></i></a>
                        <a class="js-change-year" data-dir="next"><i class="icon-right-dir"></i></a>
                    </div>
                </div>
                <div class="cal-year__months"><a class="js-month cal-year__month cal-year__month--empty ui-droppable"
                                                 data-year="2016" data-month="0">
                        <div class="cal-year__month-label">Jan</div>
                        <div class="cal-year__month-count">0</div>
                    </a><a class="js-month cal-year__month cal-year__month--empty ui-droppable" data-year="2016"
                           data-month="1">
                        <div class="cal-year__month-label">Feb</div>
                        <div class="cal-year__month-count">0</div>
                    </a><a class="js-month cal-year__month cal-year__month--empty ui-droppable" data-year="2016"
                           data-month="2">
                        <div class="cal-year__month-label">Mar</div>
                        <div class="cal-year__month-count">0</div>
                    </a><a class="js-month cal-year__month cal-year__month--empty ui-droppable" data-year="2016"
                           data-month="3">
                        <div class="cal-year__month-label">Apr</div>
                        <div class="cal-year__month-count">0</div>
                    </a><a class="js-month cal-year__month cal-year__month--selected cal-year__month--now ui-droppable"
                           data-year="2016" data-month="4">
                        <div class="cal-year__month-label">May</div>
                        <div class="cal-year__month-count">8</div>
                    </a><a class="js-month cal-year__month cal-year__month--empty ui-droppable" data-year="2016"
                           data-month="5">
                        <div class="cal-year__month-label">Jun</div>
                        <div class="cal-year__month-count">0</div>
                    </a><a class="js-month cal-year__month cal-year__month--empty ui-droppable" data-year="2016"
                           data-month="6">
                        <div class="cal-year__month-label">Jul</div>
                        <div class="cal-year__month-count">0</div>
                    </a><a class="js-month cal-year__month  ui-droppable" data-year="2016" data-month="7">
                        <div class="cal-year__month-label">Aug</div>
                        <div class="cal-year__month-count">1</div>
                    </a><a class="js-month cal-year__month cal-year__month--empty ui-droppable" data-year="2016"
                           data-month="8">
                        <div class="cal-year__month-label">Sep</div>
                        <div class="cal-year__month-count">0</div>
                    </a><a class="js-month cal-year__month cal-year__month--empty ui-droppable" data-year="2016"
                           data-month="9">
                        <div class="cal-year__month-label">Oct</div>
                        <div class="cal-year__month-count">0</div>
                    </a><a class="js-month cal-year__month cal-year__month--empty ui-droppable" data-year="2016"
                           data-month="10">
                        <div class="cal-year__month-label">Nov</div>
                        <div class="cal-year__month-count">0</div>
                    </a><a class="js-month cal-year__month cal-year__month--empty ui-droppable" data-year="2016"
                           data-month="11">
                        <div class="cal-year__month-label">Dec</div>
                        <div class="cal-year__month-count">0</div>
                    </a></div>
            </div>
        </div>
        <div class="calendar__scroll-cont">
            <table class="calendar" cellpadding="0" cellspacing="0">
                <thead>
                <tr>
                    <td class="calendar__day-of-week">Mon</td>
                    <td class="calendar__day-of-week">Tue</td>
                    <td class="calendar__day-of-week">Wed</td>
                    <td class="calendar__day-of-week">Thu</td>
                    <td class="calendar__day-of-week">Fri</td>
                    <td class="calendar__day-of-week">Sat</td>
                    <td class="calendar__day-of-week">Sun</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-04-18"><span class="calendar__date">18</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-04-19"><span class="calendar__date">19</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-04-20"><span class="calendar__date">20</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-04-21"><span class="calendar__date">21</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-04-22"><span class="calendar__date">22</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-04-23"><span class="calendar__date">23</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-04-24"><span class="calendar__date">24</span>
                        <ul></ul>
                    </td>
                </tr>
                <tr>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-04-25"><span class="calendar__date">25</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-04-26"><span class="calendar__date">26</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-04-27"><span class="calendar__date">27</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-04-28"><span class="calendar__date">28</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-04-29"><span class="calendar__date">29</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-04-30"><span class="calendar__date">30</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-01"><span
                            class="calendar__date">1</span>
                        <ul></ul>
                    </td>
                </tr>
                <tr>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-02"><span
                            class="calendar__date">2</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-03"><span
                            class="calendar__date">3</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-04"><span
                            class="calendar__date">4</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-05"><span
                            class="calendar__date">5</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-06"><span
                            class="calendar__date">6</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-07"><span
                            class="calendar__date">7</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-08"><span
                            class="calendar__date">8</span>
                        <ul></ul>
                    </td>
                </tr>
                <tr>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-09"><span
                            class="calendar__date">9</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-10"><span
                            class="calendar__date">10</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-11"><span
                            class="calendar__date">11</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-12"><span
                            class="calendar__date">12</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-13"><span
                            class="calendar__date">13</span>
                        <ul>
                            <li><a class="calendar__node js-draggable-calendar-node hint--top  ui-draggable"
                                   data-node-id="68492" href="#nodes/68492" data-hint="Goal: Inbox"> anasun\sadasd7 </a>
                            </li>
                            <li><a class="calendar__node js-draggable-calendar-node hint--top  ui-draggable"
                                   data-node-id="68495" href="#nodes/68495" data-hint="Goal: Inbox"> bdfgdfgdfg </a>
                            </li>
                            <li><a class="calendar__node js-draggable-calendar-node hint--top  ui-draggable"
                                   data-node-id="69273" href="#nodes/69273" data-hint="Goal: Inbox"> ddd ^ </a></li>
                            <li><a class="calendar__node js-draggable-calendar-node hint--top  ui-draggable"
                                   data-node-id="69276" href="#nodes/69276" data-hint="Goal: Inbox"> asd </a></li>
                            <li><a class="calendar__node js-draggable-calendar-node hint--top  ui-draggable"
                                   data-node-id="69285" href="#nodes/69285" data-hint="Goal: Inbox"> ^ ggg </a></li>
                        </ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-14"><span
                            class="calendar__date">14</span>
                        <ul>
                            <li><a class="calendar__node js-draggable-calendar-node hint--top  ui-draggable"
                                   data-node-id="68494" href="#nodes/68494" data-hint="Goal: Inbox"> wdeffdfsdfsdfsdfs
                                    ^ </a></li>
                        </ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-15"><span
                            class="calendar__date">15</span>
                        <ul></ul>
                    </td>
                </tr>
                <tr>
                    <td class="js-droppable-calendar-day calendar__day  calendar__day--today ui-droppable"
                        data-date="2016-05-16"><span class="calendar__date">16</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-17"><span
                            class="calendar__date">17</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-18"><span
                            class="calendar__date">18</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-19"><span
                            class="calendar__date">19</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-20"><span
                            class="calendar__date">20</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-21"><span
                            class="calendar__date">21</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-22"><span
                            class="calendar__date">22</span>
                        <ul></ul>
                    </td>
                </tr>
                <tr>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-23"><span
                            class="calendar__date">23</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-24"><span
                            class="calendar__date">24</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-25"><span
                            class="calendar__date">25</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-26"><span
                            class="calendar__date">26</span>
                        <ul>
                            <li><a class="calendar__node js-draggable-calendar-node hint--top  ui-draggable"
                                   data-node-id="69274" href="#nodes/69274" data-hint="Goal: Inbox"> ^ </a></li>
                        </ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-27"><span
                            class="calendar__date">27</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-28"><span
                            class="calendar__date">28</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-29"><span
                            class="calendar__date">29</span>
                        <ul></ul>
                    </td>
                </tr>
                <tr>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-30"><span
                            class="calendar__date">30</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day   ui-droppable" data-date="2016-05-31"><span
                            class="calendar__date">31</span>
                        <ul>
                            <li><a class="calendar__node js-draggable-calendar-node hint--top  ui-draggable"
                                   data-node-id="69261" href="#nodes/69261" data-hint="Goal: Inbox"> sss </a></li>
                        </ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-06-01"><span class="calendar__date">1</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-06-02"><span class="calendar__date">2</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-06-03"><span class="calendar__date">3</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-06-04"><span class="calendar__date">4</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-06-05"><span class="calendar__date">5</span>
                        <ul></ul>
                    </td>
                </tr>
                <tr>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-06-06"><span class="calendar__date">6</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-06-07"><span class="calendar__date">7</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-06-08"><span class="calendar__date">8</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-06-09"><span class="calendar__date">9</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-06-10"><span class="calendar__date">10</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-06-11"><span class="calendar__date">11</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-06-12"><span class="calendar__date">12</span>
                        <ul></ul>
                    </td>
                </tr>
                <tr>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-06-13"><span class="calendar__date">13</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-06-14"><span class="calendar__date">14</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-06-15"><span class="calendar__date">15</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-06-16"><span class="calendar__date">16</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-06-17"><span class="calendar__date">17</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-06-18"><span class="calendar__date">18</span>
                        <ul></ul>
                    </td>
                    <td class="js-droppable-calendar-day calendar__day calendar__day--other-month  ui-droppable"
                        data-date="2016-06-19"><span class="calendar__date">19</span>
                        <ul></ul>
                    </td>
                </tr>
                <tr></tr>
                </tbody>
            </table>
        </div>
    </div>
</div>