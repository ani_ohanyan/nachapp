<?php
    use yii\helpers\Url;
?>
<div id="js-page-main" class="t-region t-region--active" style="display: block;">
    <div>
        <div class="padded-content"><h2 class="heading--divider" style="margin-top: 10px;">Graphs</h2>
            <a href="<?= Url::home().'graphs/categories'?>" class="graph-box">
                <img src="//images.nachapp.com/bundles/nachgoal/images/misc/graphs/categories.png?v148">
                <span  class="graph-box__title">Categories</span>
            </a>
            <a href="<?=Url::home().'graphs/goals'?>" class="graph-box">
                <img  src="//images.nachapp.com/bundles/nachgoal/images/misc/graphs/completed.png?v148">
                <span class="graph-box__title">Completed goals</span>
            </a>
            <a href="#" class="graph-box">
                <img src="//images.nachapp.com/bundles/nachgoal/images/misc/graphs/next.png?v148">
                <span class="graph-box__title">Next steps</span>
            </a>
        </div>
    </div>
</div>