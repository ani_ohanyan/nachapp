<?php
    use yii\helpers\Url;
?>
<div>
    <div class="padded-content">
        <h2 class="heading--divider" style="margin-bottom: 15px; margin-top: 10px;">
            <a href='<?=Url::home().'graphs'?>'>Graphs</a> > Completed goals
        </h2>
        <ul class="js-recent-goals">
            <li>You haven't completed any goals yet.</li>
        </ul>
    </div>
</div>
