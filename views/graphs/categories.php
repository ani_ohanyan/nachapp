<?php
    use yii\helpers\Url;
?>
<div id="js-app">
    <div id="js-page-main" class="t-region t-region--active">
        <div>
            <div class="padded-content">
                <h2 class="heading--divider" style="margin-bottom: 20px; margin-top: 10px;">
                    <a class="js-refresh refresh" style="display: none;"><i class="icon-arrows-cw"></i>refresh</a>
                    <a href="<?= Url::home().'graphs'?>">Graphs</a> &#62; Category Breakdown <a href="<?= Url::home().'categories'?>" class="hint--right" data-hint="Manage categories">
                        <i  class="icon-pencil"></i>
                    </a>
                </h2>
                <p>You haven't completed any steps linked to your <a href="<?= Url::home().'categories'?>">categories</a> yet.</p>
                <p>Complete some steps on your to-do list which have categories assigned, and you'll start to see graphs
                    here.</p>
            </div>
        </div>
    </div>
</div>