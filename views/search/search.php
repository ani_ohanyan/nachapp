<div id="js-page-search" class="t-region t-region--active" style="display: block;">
    <div>
        <div class="padded-content" style="padding-bottom: 300px">
            <div style="margin: 30px 0 10px">
                <span style="display: block; margin-bottom: 3px; color: #999; font-size: 14px">Search your data
                    <a class="help-prompt js-help-prompt-search"><i class="icon-help-circled"></i></a>
                </span>
                <div class="js-help-popout-search dropdown dropdown--help-snip help-snip" style="display: none;">
                    <h3 class="help-snip__heading"><i class="icon-search"></i> Searching </h3>
                    <div class="help-snip__content">
                        <p>Type a query below to search through <em>everything</em> you've
                            stored on the site - goals, steps, notes, trackers and attachments - even those which have
                            long-since been archived.
                        </p>
                        <p>For those looking to make more advanced searches, several search modifiers are supported, and
                            specific fields can be searched too. See
                            <a href="/help/guides/searching" target="_blank">this guide</a> for more details.
                        </p>
                    </div>
                </div>
                <table class="search-query" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td><input type="text" class="js-query search-query__input"></td>
                            <td class="search-query__col2">
                                <a class="js-go button button--cta search-query__button">
                                    <i class="icon-search"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <span class="js-enter-hint search__enter-hint"> </span>
            </div>
            <span class="js-results-count search__results-count"></span>
            <ul class="js-results search-results"></ul>
            <a class="button search__more js-load-more" style="display: none">Show more</a>
        </div>
    </div>
</div>